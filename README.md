# Aplikasi Kegiatan Gereja

Aplikasi Kegiatan Gereja adalah aplikasi untuk mendata kehadiran jemaat dalam sebuah kegiatan. Dengan adanya aplikasi ini, kami berharap dapat membantu gereja dalam memahami pola kehadiran dan keaktifan jemaat dalam mengikuti kegiatan gereja yang diselenggarakan. 

Aplikasi ini kami buat dengan cinta sehingga kakak dapat menggunakan aplikasi ini `secara gratis` untuk gereja kakak.

Aplikasi ini cocok digunakan oleh gereja yang berdenominasi GPIB. Walaupun demikian, kakak diperkenankan untuk mengubah / memodifikasi aplikasi ini sesuai dengan gereja kakak sendiri.

Kami juga bersedia untuk membantu jika kakak butuh tim pengembang aplikasi dalam mengembangkan aplikasi ini agar sesuai dengan kebutuhan gereja kakak.

**Penting**

Aplikasi ini hanya bisa dijalankan jika kakak sudah menginstall dan menggunakan aplikasi database jemaat dari kami.
Jika belum, kakak bisa mengunduh aplikasi database jemaat `secara gratis` disini: [Unduh Aplikasi Database Jemaat](https://bitbucket.org/bhaga_dev/aplikasi-database-jemaat/)

## Fitur
- Scan barcode dari kartu tanda jemaat
- Dapat digunakan untuk kegiatan ibadah maupun non ibadah
- Dapat menginputkan data jemaat sebagai tamu. Tamu adalah jemaat yang tidak terdaftar sebagai warga gereja tetapi hadir dalam kegiatan (ibadah maupun non ibadah)
- Laporan summary kehadiran jemaat
- Laporan tren kehadiran jemaat
- Laporan data kehadiran jemaat
- Laporan keaktifan jemaat

## Demo Program
Sabar ya kak, akan segera kami siapkan.

## Instalasi

Aplikasi Kegiatan Gereja ini dibangun dengan berbasis web. Dengan kata lain, kakak bisa menginstall aplikasi ini di server atau web hosting agar dapat diakses secara online. 

Selain itu, aplikasi ini juga bisa berjalan secara offline di komputer lokal kakak sendiri. Berikut ini adalah panduan untuk instalasi aplikasi kegiatan gereja untuk penggunaan secara offline: [Panduan Instalasi Aplikasi Kegiatan Gereja](https://bitbucket.org/bhaga_dev/aplikasi-kegiatan-gereja/wiki/)

## Ide dan Saran Pengembangan
Kami akan sangat senang jika kakak mengirimkan feedback kepada kami terkait ide dan saran untuk fitur-fitur aplikasi ini selanjutnya. Selain itu, kami terbuka untuk menerima laporan bug atau error aplikasi ini sehingga kami juga dapat memperbaikinya. 
Kakak dapat mengirimkan saran / ide pengembangan serta bug atau error aplikasi melalui di halaman berikut: [Issue Tracker](https://bitbucket.org/bhaga_dev/aplikasi-kegiatan-gereja/issues/new)

## Developer
- Bhaga Yanuardo Missa
- Email: kotaksurat@bhaga.id
- Website: [www.bhaga.id](https://www.bhaga.id)

## Lisensi
[Creative Commons CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.id)
