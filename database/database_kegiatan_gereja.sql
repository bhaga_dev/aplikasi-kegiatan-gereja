/*
SQLyog Ultimate v12.5.1 (64 bit)
MySQL - 10.4.6-MariaDB : Database - kegiatan_gereja_sementara
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `hak_akses_menu` */

DROP TABLE IF EXISTS `hak_akses_menu`;

CREATE TABLE `hak_akses_menu` (
  `id_menu` varchar(10) NOT NULL,
  `id_jns_admin` int(11) NOT NULL,
  PRIMARY KEY (`id_menu`,`id_jns_admin`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hak_akses_menu` */

insert  into `hak_akses_menu`(`id_menu`,`id_jns_admin`) values 
('1',1),
('2',1),
('3',1),
('4',1),
('5',1),
('5.1',1),
('5.2',1),
('5.3',1),
('5.4',1),
('50',1),
('50.1',1),
('50.2',1),
('50.3',1),
('50.4',1);

/*Table structure for table `jns_admin` */

DROP TABLE IF EXISTS `jns_admin`;

CREATE TABLE `jns_admin` (
  `id_jns_admin` int(11) NOT NULL AUTO_INCREMENT,
  `jns_admin` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `active` int(1) DEFAULT 1,
  `user_create` int(11) DEFAULT NULL,
  `time_create` timestamp NOT NULL DEFAULT current_timestamp(),
  `user_update` int(11) DEFAULT NULL,
  `time_update` datetime DEFAULT NULL,
  PRIMARY KEY (`id_jns_admin`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

/*Data for the table `jns_admin` */

insert  into `jns_admin`(`id_jns_admin`,`jns_admin`,`active`,`user_create`,`time_create`,`user_update`,`time_update`) values 
(1,'Administrator',1,1,'2000-01-01 00:00:00',1,'2000-01-01 00:00:00');

/*Table structure for table `keaktifan_jemaat` */

DROP TABLE IF EXISTS `keaktifan_jemaat`;

CREATE TABLE `keaktifan_jemaat` (
  `id_keaktifan_jemaat` int(11) NOT NULL AUTO_INCREMENT,
  `kode_jemaat` varchar(200) DEFAULT NULL,
  `nama_jemaat` varchar(200) DEFAULT NULL,
  `id_sektor` int(11) DEFAULT NULL,
  `nama_sektor` varchar(200) DEFAULT NULL,
  `jml_ikut_kegiatan` int(11) NOT NULL DEFAULT 0,
  `jml_kegiatan_terselenggara` int(11) NOT NULL DEFAULT 0,
  `persentase` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_keaktifan_jemaat`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `keaktifan_jemaat` */

/*Table structure for table `kegiatan_kategori` */

DROP TABLE IF EXISTS `kegiatan_kategori`;

CREATE TABLE `kegiatan_kategori` (
  `id_kegiatan_kategori` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kegiatan_kategori` varchar(200) NOT NULL,
  `tipe_kegiatan` enum('IBADAH','NON IBADAH') NOT NULL,
  `active` int(1) DEFAULT 1,
  `user_create` int(11) DEFAULT NULL,
  `time_create` timestamp NOT NULL DEFAULT current_timestamp(),
  `time_update` datetime NOT NULL,
  `user_update` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_kegiatan_kategori`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `kegiatan_kategori` */

/*Table structure for table `kegiatan` */

DROP TABLE IF EXISTS `kegiatan`;

CREATE TABLE `kegiatan` (
  `id_kegiatan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kegiatan` varchar(200) NOT NULL,
  `waktu_mulai` datetime NOT NULL,
  `waktu_selesai` datetime NOT NULL,
  `tempat_kegiatan` text DEFAULT NULL,
  `id_kegiatan_kategori` int(11) NOT NULL,
  `id_kegiatan_parent` int(11) DEFAULT NULL COMMENT 'Jika memiliki id_kegiatan_parent, maka kegiatan ini adalah kegiatan berulang',
  `tipe_berulang` enum('Setiap Hari','Setiap Minggu','Setiap Bulan','Setiap Tahun','Tidak') DEFAULT NULL,
  `active` int(1) DEFAULT 1,
  `user_create` int(11) DEFAULT NULL,
  `time_create` timestamp NOT NULL DEFAULT current_timestamp(),
  `time_update` datetime NOT NULL,
  `user_update` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_kegiatan`),
  KEY `id_kegiatan_kategori` (`id_kegiatan_kategori`),
  CONSTRAINT `kegiatan_ibfk_1` FOREIGN KEY (`id_kegiatan_kategori`) REFERENCES `kegiatan_kategori` (`id_kegiatan_kategori`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `kegiatan` */

/*Table structure for table `log_crud` */

DROP TABLE IF EXISTS `log_crud`;

CREATE TABLE `log_crud` (
  `id_log_crud` int(11) NOT NULL AUTO_INCREMENT,
  `tabel_crud` varchar(50) NOT NULL,
  `tipe_crud` varchar(15) NOT NULL COMMENT 'INSERT, UPDATE, DELETE',
  `codition_crud` text DEFAULT NULL COMMENT 'JSON DATA',
  `before_crud` text DEFAULT NULL COMMENT 'JSON DATA',
  `after_crud` text DEFAULT NULL COMMENT 'JSON DATA',
  `user_executor` varchar(45) NOT NULL COMMENT 'ID EXECUTOR',
  `table_executor` varchar(70) DEFAULT NULL COMMENT 'EXECUTOR FROM WHERE',
  `time_execute` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id_log_crud`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `log_crud` */

/*Table structure for table `menu` */

DROP TABLE IF EXISTS `menu`;

CREATE TABLE `menu` (
  `id_menu` varchar(10) NOT NULL,
  `nama_menu` varchar(25) DEFAULT NULL,
  `icon_menu` varchar(20) DEFAULT NULL,
  `url_menu` varchar(100) DEFAULT NULL,
  `parent_menu` varchar(10) DEFAULT NULL,
  `urutan_menu` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_menu`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `menu` */

insert  into `menu`(`id_menu`,`nama_menu`,`icon_menu`,`url_menu`,`parent_menu`,`urutan_menu`) values 
('1','Home','fa fa-home','page/home',NULL,1),
('2','Kategori Kegiatan','fa fa-bars','page/kegiatan_kategori',NULL,2),
('3','Kegiatan','fa fa-briefcase','page/kegiatan',NULL,3),
('4','Presensi Kegiatan','fa fa-calendar','page/presensi_kegiatan',NULL,4),
('5','Laporan','fa fa-print','#',NULL,5),
('5.1','Summary Kehadiran',NULL,'page/summary_kehadiran','5',1),
('5.2','Tren Kehadiran',NULL,'page/tren_kehadiran_jemaat','5',2),
('5.3','Data Presensi Kegiatan',NULL,'page/data_presensi_kegiatan','5',3),
('5.4','Keaktifan Jemaat',NULL,'page/keaktifan_jemaat','5',4),
('50','Setting','fa fa-wrench','#',NULL,50),
('50.1','Tipe Administrator',NULL,'page/tipe_admin','50',1),
('50.2','Hak Akses Menu',NULL,'page/hak_akses_menu','50',2),
('50.3','Administrator',NULL,'page/administrator','50',3),
('50.4','Ganti Password',NULL,'page/ganti_password','50',4);

/*Table structure for table `mst_admin` */

DROP TABLE IF EXISTS `mst_admin`;

CREATE TABLE `mst_admin` (
  `id_admin` int(11) NOT NULL AUTO_INCREMENT,
  `nama_admin` varchar(100) DEFAULT NULL,
  `username_admin` varchar(50) DEFAULT NULL,
  `password_admin` varchar(70) DEFAULT NULL,
  `email_admin` varchar(100) DEFAULT NULL,
  `status_admin` varchar(1) DEFAULT NULL COMMENT 'A = Aktif, N = Non Aktif, D = Deleted',
  `id_jns_admin` int(11) DEFAULT NULL,
  `foto_admin` text DEFAULT NULL,
  PRIMARY KEY (`id_admin`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `mst_admin` */

insert  into `mst_admin`(`id_admin`,`nama_admin`,`username_admin`,`password_admin`,`email_admin`,`status_admin`,`id_jns_admin`,`foto_admin`) values 
(1,'Super administrator','sys_admin','be0debf79f63e1f67232e6978ef51d0b','admin@admin.com','A',1,NULL);

/*Table structure for table `presensi_kegiatan` */

DROP TABLE IF EXISTS `presensi_kegiatan`;

CREATE TABLE `presensi_kegiatan` (
  `id_presensi_kegiatan` int(11) NOT NULL AUTO_INCREMENT,
  `id_kegiatan` int(11) NOT NULL,
  `id_jemaat` int(11) DEFAULT NULL,
  `kode_jemaat` varchar(100) DEFAULT NULL,
  `nama_jemaat` varchar(200) DEFAULT NULL,
  `id_sektor` int(11) DEFAULT NULL,
  `nama_sektor` varchar(100) DEFAULT NULL,
  `jns_kelamin` varchar(1) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `id_pelkat` int(11) DEFAULT NULL,
  `nama_pelkat` varchar(200) DEFAULT NULL,
  `singkatan_pelkat` varchar(50) DEFAULT NULL,
  `keterangan` varchar(200) DEFAULT NULL,
  `active` int(1) DEFAULT 1,
  `user_create` int(11) DEFAULT NULL,
  `time_create` timestamp NOT NULL DEFAULT current_timestamp(),
  `time_update` datetime NOT NULL,
  `user_update` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_presensi_kegiatan`),
  KEY `id_kegiatan` (`id_kegiatan`),
  CONSTRAINT `presensi_kegiatan_ibfk_1` FOREIGN KEY (`id_kegiatan`) REFERENCES `kegiatan` (`id_kegiatan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `presensi_kegiatan` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
