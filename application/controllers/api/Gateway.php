<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

class Gateway extends MY_Controller {
    var $basic_token = 'IASJD9A8DADHADA98ADY23E09U0SUC8023UW09Y0SCS09CA90E7J';

    function __construct(){
        parent::__construct();
        $this->load->model("Mst_admin_model",'admin');
        $this->load->model("Menu_model",'menu');
    }

    function genToken($table, $where){
        $return = array();
        $this->admin->tabel = $table;
        $select = "id_admin,
                        nama_admin,
                        username_admin,
                        password_admin,
                        status_admin,
                        id_jns_admin,
                        (select jns_admin from jns_admin where id_jns_admin=mst_admin.id_jns_admin) jns_admin
                        ";
        $send_data = array('select' => $select, 'where' => $where);
        $data = $this->admin->load_data($send_data);
        if($data->num_rows() > 0){
            $load_data = $this->getTokenMobile($data->row(), 'LOAD_DATA', true);
            $send_data = $this->getTokenMobile($data->row(), 'SEND_DATA', true);

            $return['sts'] = true;
            $return['load_data'] = $load_data;
            $return['send_data'] = $send_data;
        }
        else{
            $return['sts'] = false;
        }

        return $return;
    }

    public function login(){
        $token = $this->input->post('token');
        $username = htmlentities($this->input->post('username'));
        $password = htmlentities($this->input->post('password'));

        $return = array();
        if($token == $this->basic_token){
            $data = $this->admin->login($username, $password, 'mobile');
            $result = $data['status'];
            if($result){
                $developer = array('nama_developer' => $this->nama_developer,
                    'nama_aplikasi' => $this->aplikasi,
                    'url_developer' => $this->url_developer);
                $instansi = array('nama_instansi' => $this->nama_instansi,
                    'alamat' => $this->alamat,
                    'telepon' => $this->telepon,
                    'email' => $this->email);
                $return['sts'] = true;
                $return['data'] = $data['data'];
                $return['developer'] = $developer;
                $return['instansi'] = $instansi;
                $return['message'] = $this->alert_text('login_berhasil')['msg'];

                $where = array('id_admin' => $data['data']->id_admin);
                $return['token'] = $this->genToken('mst_admin', $where);

            }
            else if($result == false){
                $return['sts'] = false;
                $return['data'] = new stdClass();
                $return['message'] = $this->alert_text('not_valid')['msg'];

            }
        }
        else{
            $return['sts'] = false;
            $return['data'] = $this->alert_text('token_invalid')['type'];
            $return['message'] = $this->alert_text('token_invalid')['msg'];
        }

        echo json_encode($return);
    }
}
?>
