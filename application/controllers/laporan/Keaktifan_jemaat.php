
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Keaktifan_jemaat extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model("kegiatan_kategori_model","kategori_kegiatan");
        $this->load->model("presensi_kegiatan_model","presensi_kegiatan");
        $this->load->model("keaktifan_jemaat_model","keaktifan_jemaat");
        $this->load->model("kegiatan_model","kegiatan");
    }

    public function index(){
        $periode_awal = htmlentities($this->input->get('periode_awal'));
        $periode_akhir = htmlentities($this->input->get('periode_akhir'));
        $id_kegiatan_kategori = $this->input->get('id_kegiatan_kategori');
        $id_pelkat = $this->input->get('id_pelkat');
        $token = $this->input->get('token');

        if($id_pelkat and is_array($id_pelkat)){
            $id_pelkat = implode(',', $id_pelkat);
        }

        if($this->validasi_login()){
            if($this->tokenStatus($token, 'LOAD_DATA') and $periode_awal and $periode_akhir){
                #mencari kategori kegiatan...
                $where_kategori = "active = 1";
                if($id_kegiatan_kategori){
                    $where_kategori .= " and id_kegiatan_kategori IN (".$id_kegiatan_kategori.")";
                }
                $data_send_kategori = array('where' => $where_kategori);
                $load_data_kategori = $this->kategori_kegiatan->load_data($data_send_kategori);
                if($load_data_kategori->num_rows() > 0){
                    $kategori_kegiatan = '';
                    if($id_kegiatan_kategori){
                        foreach ($load_data_kategori->result() as $kategori){
                            $kategori_kegiatan .= $kategori->nama_kegiatan_kategori.', ';
                        }
                        $kategori_kegiatan = rtrim($kategori_kegiatan, ', ');
                    }
                    else{
                        $kategori_kegiatan = 'Semua';
                    }

                    $tgl_pecah = explode('-', $periode_awal);
                    $periode_awal = $tgl_pecah[0].' '.$this->bulan((int) $tgl_pecah[1]).' '.$tgl_pecah[2];
                    $tgl_pecah = explode('-', $periode_akhir);
                    $periode_akhir = $tgl_pecah[0].' '.$this->bulan((int) $tgl_pecah[1]).' '.$tgl_pecah[2];

                    #mendapatkan data jemaat...
                    #login first...
                    $api = $this->database_jemaat()['api'].'gateway/login';
                    $data = array(
                        'token' => 'IASJD9A8DADHADA98ADY23E09U0SUC8023UW09Y0SCS09CA90E7J',
                        'username' => $this->database_jemaat()['username'],
                        'password' => $this->database_jemaat()['password'],
                    );
                    $login_api = json_decode($this->http_request_builder($data, $api));
                    if($login_api->sts){
                        #cari pelkat...
                        $pelkat = 'Semua';
                        if($id_pelkat){
                            $api = $this->database_jemaat()['api'].'pelkat/load_data';
                            $filter_pelkat = array(
                                'token' => $login_api->token->load_data,
                                'id_pelkat' => $id_pelkat
                            );
                            $data_pelkat = json_decode($this->http_request_builder($filter_pelkat, $api));
                            if($data_pelkat->sts){
                                $pelkat = '';
                                $data_pelkat = $data_pelkat->data;
                                for($i = 0; $i < count($data_pelkat); $i++){
                                    $pelkat .= $data_pelkat[$i]->nama_pelkat.', ';
                                }
                                $pelkat = rtrim($pelkat, ', ');
                            }
                        }

                        if($id_pelkat){
                            $data_jemaat['id_pelkat'] = $id_pelkat;
                        }

                        #load data keaktifan jemaat...
                        $order_aktif = "id_sektor, jml_ikut_kegiatan, kode_jemaat ASC";
                        $data_send_aktif = array('order' => $order_aktif);
                        $load_data_aktif = $this->keaktifan_jemaat->load_data($data_send_aktif);

                        $konten = array(
                            'kategori_kegiatan' => $kategori_kegiatan,
                            'pelkat' => $pelkat,
                            'keaktifan_jemaat' => $load_data_aktif,
                            'periode_awal' => $periode_awal,
                            'periode_akhir' => $periode_akhir,
                        );
                        $this->load->view('laporan/keaktifan_jemaat_print', $this->data_halaman($konten));

                    }
                    else{
                        $this->load->view('errors/404', $this->data_halaman());
                    }
                }
                else{
                    $this->load->view('errors/404', $this->data_halaman());
                }
            }
        }
    }

    public function jml_jemaat(){
        $id_pelkat = $this->input->post('id_pelkat');
        $token = $this->input->post('token');

        if($this->validasi_login()){
            if($this->tokenStatus($token, 'LOAD_DATA')){
                #login first...
                $api = $this->database_jemaat()['api'].'gateway/login';
                $data = array(
                    'token' => 'IASJD9A8DADHADA98ADY23E09U0SUC8023UW09Y0SCS09CA90E7J',
                    'username' => $this->database_jemaat()['username'],
                    'password' => $this->database_jemaat()['password'],
                );
                $login_api = json_decode($this->http_request_builder($data, $api));
                if($login_api->sts){
                    #cari data jemaat...
                    $api = $this->database_jemaat()['api'].'jemaat/jml_individu_jemaat';
                    $data_jemaat = array(
                        'token' => $login_api->token->load_data
                    );
                    if($id_pelkat){
                        $data_jemaat['id_pelkat'] = $id_pelkat;
                    }

                    $jemaat = json_decode($this->http_request_builder($data_jemaat, $api));
                    if($jemaat->sts) {
                        $jml_jemaat = $jemaat->data;
                        $result = array('sts' => true, 'result' => $jml_jemaat);

                        #reset database...
                        $str = "delete from keaktifan_jemaat";
                        $this->keaktifan_jemaat->query($str);
                        $str = "alter table keaktifan_jemaat auto_increment = 1";
                        $this->keaktifan_jemaat->query($str);
                    }
                    else{
                        $result['sts'] = 'gagal';
                    }
                }
                else{
                    $result['sts'] = 'gagal';
                }
                echo json_encode($result);
            }
        }
    }


    public function hitung_keaktifan_jemaat(){
        $periode_awal = htmlentities($this->input->post('periode_awal'));
        $periode_akhir = htmlentities($this->input->post('periode_akhir'));
        $id_kegiatan_kategori = htmlentities($this->input->post('id_kegiatan_kategori'));
        $id_pelkat = htmlentities($this->input->post('id_pelkat'));
        $page = htmlentities($this->input->post('page'));
        $jml_data = htmlentities($this->input->post('jml_data'));
        $token = $this->input->post('token');

        if($this->validasi_login()){
            if($this->tokenStatus($token, 'LOAD_DATA') and $periode_awal and $periode_akhir){
                $tgl_pecah = explode('-', $periode_awal);
                $periode_awal_balik = $tgl_pecah[2].'-'.$tgl_pecah[1].'-'.$tgl_pecah[0];

                $tgl_pecah = explode('-', $periode_akhir);
                $periode_akhir_balik = $tgl_pecah[2].'-'.$tgl_pecah[1].'-'.$tgl_pecah[0];

                #menghitung total kegiatan yang diselenggarakan
                $where_selenggara = "active = 1 and date_format(waktu_mulai, '%Y-%m-%d') between '".$periode_awal_balik."' and '".$periode_akhir_balik."'";
                if($id_kegiatan_kategori){
                    $where_selenggara .= " and id_kegiatan_kategori IN (".$id_kegiatan_kategori.")";
                }
                $jml_kegiatan_terselenggara = $this->kegiatan->select_count($where_selenggara);

                #mendapatkan data jemaat...
                #login first...
                $api = $this->database_jemaat()['api'].'gateway/login';
                $data = array(
                    'token' => 'IASJD9A8DADHADA98ADY23E09U0SUC8023UW09Y0SCS09CA90E7J',
                    'username' => $this->database_jemaat()['username'],
                    'password' => $this->database_jemaat()['password'],
                );
                $login_api = json_decode($this->http_request_builder($data, $api));
                if($login_api->sts){
                    #cari data jemaat...
                    $api = $this->database_jemaat()['api'].'jemaat/individu_jemaat';
                    $data_jemaat = array(
                        'token' => $login_api->token->load_data,
                        'jml_data' => $jml_data,
                        'page' => $page
                    );
                    if($id_pelkat){
                        $data_jemaat['id_pelkat'] = $id_pelkat;
                    }

                    $jemaat = json_decode($this->http_request_builder($data_jemaat, $api));
                    if($jemaat->sts){
                        $data_jemaat = $jemaat->data;
                        #mencari keaktifan jemaat...
                        for($i = 0; $i < count($data_jemaat); $i++){
                            $join_hitung[0] = array('tabel' => 'kegiatan', 'relation' => 'kegiatan.id_kegiatan = presensi_kegiatan.id_kegiatan', 'direction' => 'left');
                            $where_hitung = "presensi_kegiatan.active = 1 and presensi_kegiatan.id_jemaat = '".$data_jemaat[$i]->id_jemaat."'";
                            if($id_kegiatan_kategori){
                                $where_hitung .= " and kegiatan.id_kegiatan_kategori IN (".$id_kegiatan_kategori.")";
                            }
                            $select = "count(-1) jml";
                            $data_send_hitung = array('select' => $select, 'where' => $where_hitung, 'join' => $join_hitung);
                            $load_data_hitung = $this->presensi_kegiatan->load_data($data_send_hitung);
                            $jml_ikut_kegiatan = $load_data_hitung->row()->jml;
                            $persentase = 0;
                            if($jml_ikut_kegiatan > 0)
                                $persentase = floor(($jml_ikut_kegiatan / $jml_kegiatan_terselenggara) * 100);
                            #menyimpan kedalam tabel supaya gampang di sortir...
                            $data_simpan = array(
                                'kode_jemaat' => $data_jemaat[$i]->kode_jemaat,
                                'nama_jemaat' => $data_jemaat[$i]->nama_jemaat,
                                'id_sektor' => $data_jemaat[$i]->id_sektor,
                                'nama_sektor' => $data_jemaat[$i]->nama_sektor,
                                'jml_ikut_kegiatan' => $jml_ikut_kegiatan,
                                'jml_kegiatan_terselenggara' => $jml_kegiatan_terselenggara,
                                'persentase' => $persentase,
                            );
                            $this->keaktifan_jemaat->save($data_simpan);
                        }

                        $return['sts'] = true;
                    }
                    else{
                        $return['sts'] = false;
                    }

                }
                else{
                    $return['sts'] = false;
                }
            }
            else{
                $return['sts'] = false;
            }

        }
        else{
            $return['sts'] = 'token_salah';
        }

        echo json_encode($return);
    }
}
