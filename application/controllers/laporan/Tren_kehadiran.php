
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tren_kehadiran extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model("kegiatan_kategori_model","kegiatan_kategori");
        $this->load->model("presensi_kegiatan_model","presensi_kegiatan");
    }

    public function index(){
        $periode_awal = htmlentities($this->input->get('periode_awal'));
        $periode_akhir = htmlentities($this->input->get('periode_akhir'));
        $id_kegiatan_kategori = htmlentities($this->input->get('id_kegiatan_kategori'));
        $token = $this->input->get('token');

        if($this->validasi_login()){
            if($this->tokenStatus($token, 'LOAD_DATA') and $periode_awal and $periode_akhir and $id_kegiatan_kategori){
                $where_kategori = array('active' => 1, 'id_kegiatan_kategori' => $id_kegiatan_kategori);
                $data_send_kategori = array('where' => $where_kategori);
                $load_data_kategori = $this->kegiatan_kategori->load_data($data_send_kategori);
                if($load_data_kategori->num_rows() > 0){
                    $kategori = $load_data_kategori->row();

                    $periode_awal = $this->reformat_date($periode_awal, '-');
                    $periode_akhir = $this->reformat_date($periode_akhir, '-');

                    $join[0] = array('tabel' => 'kegiatan', 'relation' => 'kegiatan.id_kegiatan = presensi_kegiatan.id_kegiatan', 'direction' => 'left');
                    $select = "DATE_FORMAT(kegiatan.waktu_mulai, '%Y-%m-%d') tanggal, COUNT(-1) jml";
                    $where = "presensi_kegiatan.active = 1 and id_kegiatan_kategori = '".$id_kegiatan_kategori."' and date_format(kegiatan.waktu_mulai, '%Y-%m-%d') between '".$periode_awal."' and '".$periode_akhir."'";
                    $group = "DATE_FORMAT(kegiatan.waktu_mulai, '%Y-%m-%d')";
                    $data_send = array('where' => $where, 'join' => $join, 'select' => $select, 'group' => $group);
                    $load_data = $this->presensi_kegiatan->load_data($data_send);

                    $tgl_pecah = explode('-', $periode_awal);
                    $periode_awal = $tgl_pecah[2].' '.$this->bulan((int) $tgl_pecah[1]).' '.$tgl_pecah[0];
                    $tgl_pecah = explode('-', $periode_akhir);
                    $periode_akhir = $tgl_pecah[2].' '.$this->bulan((int) $tgl_pecah[1]).' '.$tgl_pecah[0];

                    $konten = array('data' => $load_data,
                        'kategori' => $kategori->nama_kegiatan_kategori,
                        'periode_awal' => $periode_awal,
                        'periode_akhir' => $periode_akhir
                    );
                    $this->load->view('laporan/tren_kehadiran_print', $this->data_halaman($konten));
                }
                else{
                    $this->load->view('errors/404', $this->data_halaman());
                }
            }
            else{
                $this->load->view('errors/404', $this->data_halaman());
            }
        }
    }

}
