
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Summary_kehadiran extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model("kegiatan_model","kegiatan");
        $this->load->model("presensi_kegiatan_model","presensi_kegiatan");
    }

    public function index(){
        $periode_awal = htmlentities($this->input->get('periode_awal'));
        $periode_akhir = htmlentities($this->input->get('periode_akhir'));
        $id_kegiatan_kategori = htmlentities($this->input->get('id_kegiatan_kategori'));
        $token = $this->input->get('token');

        if($this->validasi_login()){
            if($this->tokenStatus($token, 'LOAD_DATA') and $periode_awal and $periode_akhir){
                $reformat_periode_awal = $this->reformat_date($periode_awal, '-');
                $reformat_periode_akhir = $this->reformat_date($periode_akhir, '-');
                $where = "active = 1 and id_kegiatan_kategori = '".$id_kegiatan_kategori."' and date_format(waktu_mulai, '%Y-%m-%d') between '".$reformat_periode_awal."' and '".$reformat_periode_akhir."'";
                $data_send = array('where' => $where);
                $load_data = $this->kegiatan->load_data($data_send);
                if($load_data->num_rows() > 0){
                    foreach($load_data->result() as $row){
                        #summary jenis kelamin
                        $select_jns_kelamin = "IFNULL(jns_kelamin, 'Tidak Diketahui / Tamu') jns_kelamin, COUNT(-1) jml";
                        $where_jns_kelamin = array('active' => 1, 'id_kegiatan' => $row->id_kegiatan);
                        $group_jns_kelamin = 'jns_kelamin';
                        $data_send_jns_kelamin = array('where' => $where_jns_kelamin, 'select' => $select_jns_kelamin, 'group' => $group_jns_kelamin);
                        $row->jns_kelamin = $this->presensi_kegiatan->load_data($data_send_jns_kelamin);

                        #summery Pelkat
                        $select_pelkat = "IFNULL(nama_pelkat, 'Tidak Diketahui / Tamu') nama_pelkat, COUNT(-1) jml";
                        $where_pelkat = array('active' => 1, 'id_kegiatan' => $row->id_kegiatan);
                        $group_pelkat = 'nama_pelkat';
                        $data_send_pelkat = array('where' => $where_pelkat, 'select' => $select_pelkat, 'group' => $group_pelkat);
                        $row->pelkat = $this->presensi_kegiatan->load_data($data_send_pelkat);

                        #summery sektor
                        $select_sektor = "IFNULL(nama_sektor, 'Tidak Diketahui / Tamu') nama_sektor, COUNT(-1) jml";
                        $where_sektor = array('active' => 1, 'id_kegiatan' => $row->id_kegiatan);
                        $group_sektor = 'nama_sektor';
                        $data_send_sektor = array('where' => $where_sektor, 'select' => $select_sektor, 'group' => $group_sektor);
                        $row->sektor = $this->presensi_kegiatan->load_data($data_send_sektor);

                    }
                }

                $tgl_pecah = explode('-', $periode_awal);
                $periode_awal = $tgl_pecah[0].' '.$this->bulan((int) $tgl_pecah[1]).' '.$tgl_pecah[2];
                $tgl_pecah = explode('-', $periode_akhir);
                $periode_akhir = $tgl_pecah[0].' '.$this->bulan((int) $tgl_pecah[1]).' '.$tgl_pecah[2];

                $konten = array('data' => $load_data, 'periode_awal' => $periode_awal, 'periode_akhir' => $periode_akhir);
                $this->load->view('laporan/summary_kehadiran_print', $this->data_halaman($konten));
            }
        }
    }

}
