
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Data_kehadiran_jemaat extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model("kegiatan_model","kegiatan");
        $this->load->model("presensi_kegiatan_model","presensi_kegiatan");
    }

    public function index(){
        $id_kegiatan = htmlentities($this->input->get('id_kegiatan'));
        $token = $this->input->get('token');

        if($this->validasi_login()){
            if($this->tokenStatus($token, 'LOAD_DATA') and $id_kegiatan){
                $where = array('active' => 1, 'id_kegiatan' => $id_kegiatan);
                $data_send = array('where' => $where);
                $load_data = $this->kegiatan->load_data($data_send);
                if($load_data->num_rows() > 0){
                    $kegiatan = $load_data->row();
                    $waktu_mulai_tgl_jam = explode(' ', $kegiatan->waktu_mulai);
                    $waktu_mulai_jam = substr($waktu_mulai_tgl_jam[1], 0, 5);
                    $waktu_mulai_pecah = explode('-', $waktu_mulai_tgl_jam[0]);

                    $waktu_akhir_tgl_jam = explode(' ', $kegiatan->waktu_selesai);
                    $waktu_akhir_jam = substr($waktu_akhir_tgl_jam[1], 0, 5);
                    $waktu_akhir_pecah = explode('-', $waktu_akhir_tgl_jam[0]);

                    $waktu = $waktu_mulai_pecah[0].' '.$this->bulan((int) $waktu_mulai_pecah[1]).' '.$waktu_mulai_pecah[2].' '.$waktu_mulai_jam.' - ';
                    $waktu .= $waktu_akhir_pecah[0].' '.$this->bulan((int) $waktu_akhir_pecah[1]).' '.$waktu_akhir_pecah[2].' '.$waktu_akhir_jam;
                    if($waktu_mulai_tgl_jam[0] == $waktu_akhir_tgl_jam[0]){
                        $waktu = $waktu_mulai_pecah[0].' '.$this->bulan((int) $waktu_mulai_pecah[1]).' '.$waktu_mulai_pecah[2].' '.$waktu_mulai_jam.' - '.$waktu_akhir_jam;
                    }

                    $kegiatan->waktu = $waktu;

                    $order_presensi = "kode_jemaat ASC";
                    $where_presensi = array('presensi_kegiatan.active' => 1, 'presensi_kegiatan.id_kegiatan' => $id_kegiatan);
                    $data_send_presensi = array('where' => $where_presensi, 'order' => $order_presensi);
                    $load_data_presensi = $this->presensi_kegiatan->load_data($data_send_presensi);

                    $konten = array('kegiatan' => $kegiatan, 'presensi' => $load_data_presensi);
                    $this->load->view('laporan/data_kehadiran_jemaat_print', $this->data_halaman($konten));
                }
                else{
                    $this->load->view('errors/404', $this->data_halaman());
                }

            }
        }
    }

}
