<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Administrator extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model("Mst_admin_model",'master_admin');
    }

    public function load_data(){
        if($this->validasi_login()){
            $data_receive = json_decode(urldecode($this->input->post('data_send')));
            $token = $data_receive->token;
            if($this->tokenStatus($token, 'LOAD_DATA')){
                $where = array('status_admin !=' => 'D');
                $join[0] = array('tabel' => 'jns_admin', 'relation' => 'jns_admin.id_jns_admin = mst_admin.id_jns_admin', 'direction' => 'left');
                $order = ' nama_admin ASC';

                $data_send = array('where' => $where, 'order' => $order, 'join' => $join);
                $load_data = $this->master_admin->load_data($data_send);
                $result = $load_data->result();
                echo json_encode($result);
            }
        }

    }

    public function simpan(){
        if($this->validasi_login()){
            $token = $this->input->post('token');
            $return = array();
            if($this->tokenStatus($token, 'SEND_DATA')){

                $id_admin = $this->input->post('id_admin');
                $nama_lengkap = $this->input->post('nama_lengkap');
                $username = $this->input->post('username');
                $password = $this->input->post('password');
                $status_admin = $this->input->post('status_admin');
                $tipe_admin = $this->input->post('tipe_admin');
                $foto_admin_blob = $this->input->post('foto_admin_blob');
                $action = $this->input->post('action');

                $file_foto = '';
                if($foto_admin_blob != ''){
                    $new_name = date('ymdHis').'_'.$this->generateRandomString(5);
                    $output = 'assets/uploads/admin/';
                    $filename = $new_name.'.jpg';
                    $file_foto = $this->base64_to_file($foto_admin_blob, $output, $filename);
                }

                #jika action memiliki value 'save' maka data akan disimpan.
                #jika action tidak memiliki value, maka akan dianggap sebagai upadate.
                if($action == 'save'){
                    #cek apakah username yang dimasukan sudah ada sebelumnya...
                    $where_username = array('username_admin' => $username);
                    $hasil_username = $this->master_admin->cek_duplikat($where_username);

                    if($hasil_username == 'available')
                        $return['sts'] = 'username_available';
                    else{
                        $data = array('nama_admin' => $nama_lengkap,
                            'username_admin' => $username,
                            'password_admin' => $password,
                            'foto_admin' => $file_foto,
                            'status_admin' => $status_admin,
                            'id_jns_admin' => $tipe_admin);
                        $exe = $this->master_admin->save($data);
                        $return['sts'] = $exe;
                    }
                }
                else{
                    #cek apakah username yang dimasukan sudah ada sebelumnya...
                    $where_username = array('username_admin' => $username,
                        'id_admin !='   => $id_admin);
                    $hasil_username = $this->master_admin->cek_duplikat($where_username);

                    if($hasil_username == 'available')
                        $return['sts'] = 'username_available';
                    else{
                        $data = array('nama_admin' => $nama_lengkap);

                        if($id_admin != 1){
                            $data['username_admin'] = $username;
                            $data['status_admin'] = $status_admin;
                            $data['id_jns_admin'] = $tipe_admin;
                        }

                        if($file_foto != ''){
                            $data['foto_admin'] = $file_foto;
                        }
                        $where = array('id_admin' => $id_admin);
                        $exe = $this->master_admin->update($data, $where);
                        $return['sts'] = $exe;
                    }
                }
            }

            echo json_encode($return);
        }
    }
    public function hapus(){
        if($this->validasi_login()){
            $data_receive = json_decode(urldecode($this->input->post('data_send')));
            $token = $data_receive->token;
            $return = array();
            if($this->tokenStatus($token, 'SEND_DATA')){
                $id_admin = htmlentities($data_receive->id_admin);

                if($id_admin != 1){
                    $data = array('status_admin' => 'D');
                    $where = array('id_admin' => $id_admin);
                    $exe = $this->master_admin->update($data, $where);
                    $return['sts'] = $exe;
                }
                else
                    $return['sts'] = 'tidak_berhak';

            }

            echo json_encode($return);
        }
    }
    public function reset_password(){
        if($this->validasi_login()){
            $data_receive = json_decode(urldecode($this->input->post('data_send')));
            $token = $data_receive->token;
            $return = array();
            if($this->tokenStatus($token, 'SEND_DATA')){
                $id_admin = htmlentities($data_receive->id_admin);
                $username = htmlentities($data_receive->username);

                $data = array('password_admin' => md5($username));
                $where = array('id_admin' => $id_admin);
                $exe = $this->master_admin->update($data, $where);
                $return['sts'] = $exe;

            }

            echo json_encode($return);
        }
    }



    public function ganti_password(){
        if($this->validasi_login()){
            $data_receive = json_decode(urldecode($this->input->post('data_send')));
            $token = $data_receive->token;
            $return = array();
            if($this->tokenStatus($token, 'SEND_DATA')){
                //cek password lama sama dengan database..
                $where = array('id_admin' => $this->session->userdata('id_admin'),
                    'password_admin' => md5($data_receive->pass_lama));
                $status = $this->master_admin->cek_duplikat($where);
                if($status == 'available'){
                    //proses update password..
                    $data = array('password_admin' => md5($data_receive->pass_baru));
                    $exe = $this->master_admin->update($data, $where);

                    //ganti session password...
                    $session = array("password_admin" => md5($data_receive->pass_baru) );
                    $this->session->set_userdata($session);

                    $return['sts'] = $exe;
                }
                else{
                    $return['sts'] = 'password_salah';
                }
            }
        }
        echo json_encode($return);

    }


}