<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page extends MY_Controller {


    function __construct(){
        parent::__construct();
    }

    function lost(){
        //halaman tidak ditemukan...
        $this->load->view('errors/404', $this->data_halaman());

    }
    public function index(){
        $this->load->view('admin_login', $this->data_halaman());
    }

    public function home(){
        $menu = 'page/home';
        if($this->validasi_controller($menu) && $this->validasi_login()){
            $this->load->view('welcome', $this->data_halaman());
        }
        else
            $this->redirect(base_url().'gateway/keluar');
    }

    public function kegiatan_kategori(){
        $menu = 'page/kegiatan_kategori';
        if($this->validasi_controller($menu) && $this->validasi_login()){

            $konten = array();
            $this->load->view('kegiatan/kegiatan_kategori', $this->data_halaman($konten));
        }
        else
            $this->redirect(base_url().'gateway/keluar');
    }

    public function kegiatan(){
        $menu = 'page/kegiatan';
        if($this->validasi_controller($menu) && $this->validasi_login()){
            $this->load->model('kegiatan_kategori_model', 'kegiatan_kategori');
            $where = array('active' => 1);  #show active data...
            $send_data = array('where' => $where);
            $kegiatan_kategori = $this->kegiatan_kategori->load_data($send_data);

            $konten = array('kegiatan_kategori' => $kegiatan_kategori);
            $this->load->view('kegiatan/kegiatan', $this->data_halaman($konten));
        }
        else
            $this->redirect(base_url().'gateway/keluar');
    }

    public function presensi_kegiatan(){
        $menu = 'page/presensi_kegiatan';
        if($this->validasi_controller($menu) && $this->validasi_login()){
            $konten = array();
            $this->load->view('kegiatan/presensi_kegiatan', $this->data_halaman($konten));
        }
        else
            $this->redirect(base_url().'gateway/keluar');
    }
    public function summary_kehadiran(){
        $menu = 'page/summary_kehadiran';
        if($this->validasi_controller($menu) && $this->validasi_login()){
            $this->load->model('kegiatan_kategori_model', 'kegiatan_kategori');
            $where = array('active' => 1);  #show active data...
            $send_data = array('where' => $where);
            $kegiatan_kategori = $this->kegiatan_kategori->load_data($send_data);

            $konten = array('kegiatan_kategori' => $kegiatan_kategori);
            $this->load->view('laporan/summary_kehadiran', $this->data_halaman($konten));
        }
        else
            $this->redirect(base_url().'gateway/keluar');
    }
    public function tren_kehadiran_jemaat(){
        $menu = 'page/tren_kehadiran_jemaat';
        if($this->validasi_controller($menu) && $this->validasi_login()){
            $this->load->model('kegiatan_kategori_model', 'kegiatan_kategori');
            $where = array('active' => 1);  #show active data...
            $send_data = array('where' => $where);
            $kegiatan_kategori = $this->kegiatan_kategori->load_data($send_data);

            $konten = array('kegiatan_kategori' => $kegiatan_kategori);
            $this->load->view('laporan/tren_kehadiran', $this->data_halaman($konten));
        }
        else
            $this->redirect(base_url().'gateway/keluar');
    }
    public function keaktifan_jemaat(){
        $menu = 'page/keaktifan_jemaat';
        if($this->validasi_controller($menu) && $this->validasi_login()){
            $this->load->model('kegiatan_kategori_model', 'kegiatan_kategori');
            $where = array('active' => 1);  #show active data...
            $send_data = array('where' => $where);
            $kegiatan_kategori = $this->kegiatan_kategori->load_data($send_data);
            $data_pelkat = '';

            # get data pelkat...
            ## login first...
            $api = $this->database_jemaat()['api'].'gateway/login';
            $data = array(
                'token' => 'IASJD9A8DADHADA98ADY23E09U0SUC8023UW09Y0SCS09CA90E7J',
                'username' => $this->database_jemaat()['username'],
                'password' => $this->database_jemaat()['password'],
            );
            $login_api = json_decode($this->http_request_builder($data, $api));
            if($login_api->sts){
                # cari data pelkat
                $api = $this->database_jemaat()['api'].'pelkat/load_data';
                $filter = array(
                    'token' => $login_api->token->load_data
                );

                $pelkat = json_decode($this->http_request_builder($filter, $api));
                if($pelkat->sts){
                    $data_pelkat = $pelkat->data;
                }
            }

            $konten = array('kegiatan_kategori' => $kegiatan_kategori, 'pelkat' => $data_pelkat);
            $this->load->view('laporan/keaktifan_jemaat', $this->data_halaman($konten));
        }
        else
            $this->redirect(base_url().'gateway/keluar');
    }
    public function data_presensi_kegiatan(){
        $menu = 'page/data_presensi_kegiatan';
        if($this->validasi_controller($menu) && $this->validasi_login()){
            $konten = array();
            $this->load->view('laporan/data_kehadiran_jemaat', $this->data_halaman($konten));
        }
        else
            $this->redirect(base_url().'gateway/keluar');
    }
    public function tipe_admin(){
        $menu = 'page/tipe_admin';
        if($this->validasi_controller($menu) && $this->validasi_login()){

            $konten = array();
            $this->load->view('setting/jns_admin', $this->data_halaman($konten));
        }
        else
            $this->redirect(base_url().'gateway/keluar');
    }

    public function hak_akses_menu(){
        $menu = 'page/hak_akses_menu';
        if($this->validasi_controller($menu) && $this->validasi_login()){
            #menampilkan tipe admin...
            $this->load->model('Jns_admin_model', 'jns_admin');
            $load_jns_admin = $this->jns_admin->load_data();
            $konten = array('jns_admin' => $load_jns_admin);

            $this->load->view('setting/hak_akses_menu', $this->data_halaman($konten));
        }
        else
            $this->redirect(base_url().'gateway/keluar');
    }
    public function hak_akses_status(){
        $menu = 'page/hak_akses_status';
        if($this->validasi_controller($menu) && $this->validasi_login()){
            #menampilkan tipe admin...
            $this->load->model('Jns_admin_model', 'jns_admin');
            $load_jns_admin = $this->jns_admin->load_data();

            #menampilkan status pasien...
            $this->load->model('Status_pasien_model', 'status_pasien');
            $load_status_pasien = $this->status_pasien->load_data();
            $konten = array('jns_admin' => $load_jns_admin,
                'status_pasien' => $load_status_pasien);

            $this->load->view('setting/hak_akses_status', $this->data_halaman($konten));
        }
        else
            $this->redirect(base_url().'gateway/keluar');
    }
    public function administrator(){
        $menu = 'page/administrator';
        if($this->validasi_controller($menu) && $this->validasi_login()){
            #menampilkan tipe admin...
            $this->load->model('Jns_admin_model', 'jns_admin');
            $load_jns_admin = $this->jns_admin->load_data();


            $konten = array('jns_admin' => $load_jns_admin);

            $this->load->view('setting/administrator', $this->data_halaman($konten));
        }
        else
            $this->redirect(base_url().'gateway/keluar');
    }
    public function ganti_password(){
        $menu = 'page/ganti_password';
        if($this->validasi_controller($menu) && $this->validasi_login()){
            $this->load->view('setting/ganti_password', $this->data_halaman());
        }
        else
            $this->redirect(base_url().'gateway/keluar');

    }

    public function generate_image(){
        function getImage_w($image,$w, $h='')
        {
            $source_path = $image;
            $w *= 1.3;
            $h *= 1.3;

            //Add file validation code here
            list($source_width, $source_height, $source_type) = getimagesize($source_path);

            if($h == '' or $h == 0)
                define('DESIRED_IMAGE_HEIGHT', round(($w/ $source_width) * $source_height));
            else
                define('DESIRED_IMAGE_HEIGHT', $h);

            define('DESIRED_IMAGE_WIDTH', $w);

            switch ($source_type) {
                case IMAGETYPE_GIF:
                    $source_gdim = imagecreatefromgif($source_path);
                    break;
                case IMAGETYPE_JPEG:
                    $source_gdim = imagecreatefromjpeg($source_path);
                    break;
                case IMAGETYPE_PNG:
                    $source_gdim = imagecreatefrompng($source_path);
                    break;
            }

            $source_aspect_ratio = $source_width / $source_height;
            $desired_aspect_ratio = DESIRED_IMAGE_WIDTH / DESIRED_IMAGE_HEIGHT;

            if ($source_aspect_ratio > $desired_aspect_ratio) {
                /*
                 * Triggered when source image is wider
                 */
                $temp_height = DESIRED_IMAGE_HEIGHT;
                $temp_width = ( int ) (DESIRED_IMAGE_HEIGHT * $source_aspect_ratio);
            } else {
                /*
                 * Triggered otherwise (i.e. source image is similar or taller)
                 */
                $temp_width = DESIRED_IMAGE_WIDTH;
                $temp_height = ( int ) (DESIRED_IMAGE_WIDTH / $source_aspect_ratio);
            }

            /*
             * Resize the image into a temporary GD image
             */

            $temp_gdim = imagecreatetruecolor($temp_width, $temp_height);
            imagecopyresampled(
                $temp_gdim,
                $source_gdim,
                0, 0,
                0, 0,
                $temp_width, $temp_height,
                $source_width, $source_height
            );

            /*
             * Copy cropped region from temporary image into the desired GD image
             */

            $x0 = ($temp_width - DESIRED_IMAGE_WIDTH) / 2;
            $y0 = ($temp_height - DESIRED_IMAGE_HEIGHT) / 2;
            $desired_gdim = imagecreatetruecolor(DESIRED_IMAGE_WIDTH, DESIRED_IMAGE_HEIGHT);
            imagecopy(
                $desired_gdim,
                $temp_gdim,
                0, 0,
                $x0, $y0,
                DESIRED_IMAGE_WIDTH, DESIRED_IMAGE_HEIGHT
            );

            /*
             * Render the image
             * Alternatively, you can save the image in file-system or database
             */

            header('Content-type: image/jpeg');
            imagejpeg($desired_gdim, null, 100);

        }

        $path = $this->input->get('path');
        $width = $this->input->get('width');
        $height = $this->input->get('height');
        echo '<img src="data:image/jpeg;base64,' . base64_encode(getImage_w($path,$width,$height)) . '">';
    }

}
