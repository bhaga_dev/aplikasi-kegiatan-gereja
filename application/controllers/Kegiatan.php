
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kegiatan extends MY_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model("Kegiatan_model","kegiatan");
	}

    public function load_data(){
        if($this->validasi_login()){
            $data_receive = json_decode(urldecode($this->input->post('data_send')));
            $token = $data_receive->token;
            if($this->tokenStatus($token, 'LOAD_DATA')){
                $filter = $data_receive->filter;
                $periode_akhir = $data_receive->periode_akhir;
                $periode_awal = $data_receive->periode_awal;

                $relation[0] = array('tabel' => 'kegiatan_kategori', 'relation' => 'kegiatan_kategori.id_kegiatan_kategori = kegiatan.id_kegiatan_kategori', 'direction' => 'left');

                $page = $data_receive->page;
                $jml_data = $data_receive->jml_data;

                $page = (empty($page) ? 1 : $page);
                $jml_data = (empty($jml_data) ? $this->qty_data : $jml_data);
                $start = ($page - 1) * $jml_data;
                $limit = $jml_data.','.$start;

                $order = "unix_timestamp(waktu_mulai) ASC";
                $where = "kegiatan.active = 1  and kegiatan_kategori.active = 1 and (kegiatan.nama_kegiatan like '%".$filter."%' or kegiatan.tempat_kegiatan like '%".$filter."%' or kegiatan_kategori.nama_kegiatan_kategori like '%".$filter."%')";
                if($periode_awal){
                    $periode_akhir = ($periode_akhir ? $periode_akhir : $periode_awal);

                    $periode_awal = $this->reformat_date($periode_awal, '-');
                    $periode_akhir = $this->reformat_date($periode_akhir, '-');

//                    if($periode_awal == $periode_akhir)
//                        $where .= " and kegiatan.waktu_mulais = '".$periode_awal."'";
//                    else
                        $where .= " and date_format(kegiatan.waktu_mulai, '%Y-%m-%d') between '".$periode_awal."' and '".$periode_akhir."'";
                }
                else{
                    $where .= " and kegiatan.waktu_selesai >= '".date('Y-m-d H:i:s')."'";
                }

                $send_data = array('where' => $where, 'join' => $relation, 'limit' => $limit, 'order' => $order);
                $load_data = $this->kegiatan->load_data($send_data);
                $result = $load_data->result();

                #find last page...
                $select = "count(-1) jml";
                $send_data = array('where' => $where, 'join' => $relation, 'select' => $select);
                $load_data = $this->kegiatan->load_data($send_data);
                $total_data = $load_data->row()->jml;

                $last_page = ceil($total_data / $jml_data);
                $result = array('result' => $result, 'last_page' => $last_page);

                echo json_encode($result);
            }
        }

    }

    public function load_select2(){
        if($this->validasi_login()){
            $token = $this->input->get('token');
            if($this->tokenStatus($token, 'LOAD_DATA')){
                $filter = $this->input->get('filter')['term'];

                $select = "id_kegiatan id, concat(nama_kegiatan, ' (', date_format(waktu_mulai, '%d-%m-%Y %H:%i'), ' - ', date_format(waktu_selesai, '%d-%m-%Y %H:%i'), ')') text";
                $where = "kegiatan.active = 1 and (kegiatan.nama_kegiatan like '%".$filter."%')";
                $data_send = array('where' => $where, 'select' => $select);
                $load_data = $this->kegiatan->load_data($data_send);
                if($load_data->num_rows() > 0){
                    $result = $load_data->result();
                }
                echo json_encode($result);
            }
        }
    }

    public function simpan(){
        if($this->validasi_login()){
            $token = $this->input->post('token');
            $return = array();
            if($this->tokenStatus($token, 'SEND_DATA')){
                $id_kegiatan = htmlentities($this->input->post('id_kegiatan'));
				$nama_kegiatan = htmlentities($this->input->post('nama_kegiatan'));
				$waktu_mulai = $this->reformat_date(htmlentities($this->input->post('waktu_mulai')), '-', true);
				$waktu_selesai = $this->reformat_date(htmlentities($this->input->post('waktu_selesai')), '-', true);
				$tempat_kegiatan = htmlentities($this->input->post('tempat_kegiatan'));
				$id_kegiatan_kategori = htmlentities($this->input->post('id_kegiatan_kategori'));
				$tipe_berulang = htmlentities($this->input->post('tipe_berulang'));
				$user_create = $this->session->userdata('id_admin');
				$time_create = date('Y-m-d H:i:s');
				$time_update = date('Y-m-d H:i:s');
				$user_update = $this->session->userdata('id_admin');

                $action = htmlentities($this->input->post('action'));

                #jika action memiliki value 'save' maka data akan disimpan.
                #jika action tidak memiliki value, maka akan dianggap sebagai upadate.
                if($action == 'save'){
                    $data = array('nama_kegiatan' => $nama_kegiatan,
								'waktu_mulai' => $waktu_mulai,
								'waktu_selesai' => $waktu_selesai,
								'tempat_kegiatan' => $tempat_kegiatan,
								'id_kegiatan_kategori' => $id_kegiatan_kategori,
								'tipe_berulang' => $tipe_berulang,
								'user_create' => $user_create,
								'time_create' => $time_create,
								'time_update' => $time_update,
								'user_update' => $user_update);
                        $exe = $this->kegiatan->save_with_autoincrement($data);
                        $return['sts'] = $exe[0];
                        $id_kegiatan = $exe[1];

                        if($tipe_berulang != 'Tidak'){
                            #update id_kegiatan parent...
                            $data_update = array('id_kegiatan_parent' => $id_kegiatan);
                            $where_update = "id_kegiatan = '".$id_kegiatan."'";
                            $this->kegiatan->update($data_update, $where_update);

                            #buat kegiatan berluang...
                            for($i = 1; $i <= 10; $i++){
                                if($tipe_berulang == 'Setiap Hari'){
                                    $satuan = 'day';
                                }
                                else if($tipe_berulang == 'Setiap Minggu'){
                                    $satuan = 'week';
                                }
                                else if($tipe_berulang == 'Setiap Bulan'){
                                    $satuan = 'month';
                                }
                                else if($tipe_berulang == 'Setiap Tahun'){
                                    $satuan = 'year';
                                }

                                $waktu_mulai = date('Y-m-d H:i:s', strtotime('+1 '.$satuan, strtotime($waktu_mulai)));
                                $waktu_selesai = date('Y-m-d H:i:s', strtotime('+1 '.$satuan, strtotime($waktu_selesai)));

                                $data = array('nama_kegiatan' => $nama_kegiatan,
                                    'waktu_mulai' => $waktu_mulai,
                                    'waktu_selesai' => $waktu_selesai,
                                    'tempat_kegiatan' => $tempat_kegiatan,
                                    'id_kegiatan_kategori' => $id_kegiatan_kategori,
                                    'tipe_berulang' => $tipe_berulang,
                                    'id_kegiatan_parent' => $id_kegiatan,
                                    'user_create' => $user_create,
                                    'time_create' => $time_create,
                                    'time_update' => $time_update,
                                    'user_update' => $user_update);
                                $this->kegiatan->save($data);
                            }
                        }
                }
                else{
                    $data = array('nama_kegiatan' => $nama_kegiatan,
								'waktu_mulai' => $waktu_mulai,
								'waktu_selesai' => $waktu_selesai,
								'tempat_kegiatan' => $tempat_kegiatan,
								'id_kegiatan_kategori' => $id_kegiatan_kategori,
								'time_update' => $time_update,
								'user_update' => $user_update);
                        $where = array('id_kegiatan' => $id_kegiatan);
                        $exe = $this->kegiatan->update($data, $where);
                        $return['sts'] = $exe;
                }
            }

            echo json_encode($return);
        }
    }
    public function hapus(){
        if($this->validasi_login()){
            $data_receive = json_decode(urldecode($this->input->post('data_send')));
            $token = $data_receive->token;
            $id_kegiatan = $data_receive->id_kegiatan;
            $tipe_hapus = $data_receive->tipe_hapus;

            $return = array();
            if($this->tokenStatus($token, 'SEND_DATA')){
                $where = array('active' => 1, 'id_kegiatan' => $id_kegiatan);
                $data_send = array('where' => $where);
                $load_data = $this->kegiatan->load_data($data_send);
                if($load_data->num_rows() > 0){
                    $kegiatan = $load_data->row();
                    $id_kegiatan_parent = $kegiatan->id_kegiatan_parent;

                    if($tipe_hapus == 'satuan_dan_masadepan'){
                        $where = array('id_kegiatan_parent' => $id_kegiatan_parent, 'id_kegiatan >=' => $id_kegiatan);
                        $exe = $this->kegiatan->soft_delete($where);
                    }
                    else if ($tipe_hapus == 'semua'){
                        $where = array('id_kegiatan_parent' => $id_kegiatan_parent);
                        $exe = $this->kegiatan->soft_delete($where);
                    }
                    else{
                        $where = array('id_kegiatan' => $id_kegiatan);
                        $exe = $this->kegiatan->soft_delete($where);
                    }
                }
                else{
                    $exe = 'gagal';
                }

                $return['sts'] = $exe;

            }

            echo json_encode($return);
        }
    }

}
