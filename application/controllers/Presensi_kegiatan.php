
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Presensi_kegiatan extends MY_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model("Presensi_kegiatan_model","presensi_kegiatan");
	}

	public function cari_jemaat(){
        if($this->validasi_login()){
            $data_receive = json_decode(urldecode($this->input->post('data_send')));
            $token = $data_receive->token;
            if($this->tokenStatus($token, 'LOAD_DATA')){
                $kode_jemaat = $data_receive->kode_jemaat;
                $id_kegiatan = $data_receive->id_kegiatan;
                $mode = $data_receive->mode;

                #login first...
                $api = $this->database_jemaat()['api'].'gateway/login';
                $data = array(
                    'token' => 'IASJD9A8DADHADA98ADY23E09U0SUC8023UW09Y0SCS09CA90E7J',
                    'username' => $this->database_jemaat()['username'],
                    'password' => $this->database_jemaat()['password'],
                );
                $login_api = json_decode($this->http_request_builder($data, $api));
                if($login_api->sts){
                    #cari data jemaat...
                    $api = $this->database_jemaat()['api'].'jemaat/individu_jemaat';
                    $data_jemaat = array(
                        'token' => $login_api->token->load_data,
                        'jml_data' => 50
                    );
                    if($mode == 'cari'){
                        $data_jemaat['pencarian'] = $kode_jemaat;
                    }
                    else{
                        $data_jemaat['id_jemaat'] = $kode_jemaat;
                    }
                    $jemaat = json_decode($this->http_request_builder($data_jemaat, $api));
                    if($jemaat->sts){
                        if(count($jemaat->data) == 1){
                            #jika data jemaat hanya satu maka langsung simpan ke presensi jemaat...
                            #cek dulu apakah sudah mendaftar atau belum...
                            $where = array('active' => 1, 'id_kegiatan' => $id_kegiatan, 'kode_jemaat' => $jemaat->data[0]->kode_jemaat);
                            $cek_presensi = $this->presensi_kegiatan->cek_duplikat($where);
                            if($cek_presensi == 'zero'){
                                $return['sts'] = $this->simpan_presensi($id_kegiatan, $jemaat->data[0]);
                            }
                            else{
                                $return['sts'] = 'duplikat_presensi';
                            }

                        }
                        else if(count($jemaat->data) > 1){
                            #jika lebih dari satu, kirim ke frontend untuk dipilih...
                            $return['sts'] = 'jemaat_banyak';
                            $return['data'] = $jemaat->data;
                        }
                        else{
                            $return['sts'] = 'tidak_ada_jemaat';
                        }
                    }
                    else{
                        $return['sts'] = $jemaat->message;
                    }
                }
                else{
                    $return['sts'] = $login_api->message;
                }
                echo json_encode($return);
            }
        }
    }
	public function simpan_tamu(){
        if($this->validasi_login()){
            $data_receive = json_decode(urldecode($this->input->post('data_send')));
            $token = $data_receive->token;
            if($this->tokenStatus($token, 'LOAD_DATA')){
                $kode_jemaat = $data_receive->kode_jemaat;
                $id_kegiatan = $data_receive->id_kegiatan;

                $jemaat = (object) array(
                  'id_jemaat' => null,
                  'kode_jemaat' => 'tamu',
                  'nama_jemaat' => $kode_jemaat,
                  'id_sektor' => null,
                  'nama_sektor' => null,
                  'jenis_kelamin' => null,
                  'tgl_lahir' => null,
                  'id_pelkat' => null,
                  'nama_pelkat' => null,
                  'singkatan_pelkat' => null,
                );

                $return['sts'] = $this->simpan_presensi($id_kegiatan, $jemaat);


                echo json_encode($return);
            }
        }
    }
    public function simpan_presensi($id_kegiatan, $jemaat){
        $data = array('id_kegiatan' => $id_kegiatan,
            'id_jemaat' => $jemaat->id_jemaat,
            'kode_jemaat' => $jemaat->kode_jemaat,
            'nama_jemaat' => $jemaat->nama_jemaat,
            'id_sektor' => $jemaat->id_sektor,
            'nama_sektor' => $jemaat->nama_sektor,
            'jns_kelamin' => $jemaat->jenis_kelamin,
            'tgl_lahir' => $jemaat->tgl_lahir,
            'id_pelkat' => $jemaat->id_pelkat,
            'nama_pelkat' => $jemaat->nama_pelkat,
            'singkatan_pelkat' => $jemaat->singkatan_pelkat,
            'user_create' => $this->session->userdata('id_admin'),
            'time_create' => date('Y-m-d H:i:s'),
            'time_update' => date('Y-m-d H:i:s'),
            'user_update' => $this->session->userdata('id_admin'));
        return $this->presensi_kegiatan->save($data);
    }

    public function load_data(){
        if($this->validasi_login()){
            $data_receive = json_decode(urldecode($this->input->post('data_send')));
            $token = $data_receive->token;
            if($this->tokenStatus($token, 'LOAD_DATA')){
                $filter = $data_receive->filter;
                $id_kegiatan = $data_receive->id_kegiatan;
                $relation[0] = array('tabel' => 'kegiatan', 'relation' => 'kegiatan.id_kegiatan = presensi_kegiatan.id_kegiatan', 'direction' => 'left');

                $page = $data_receive->page;
                $jml_data = $data_receive->jml_data;

                $page = (empty($page) ? 1 : $page);
                $jml_data = (empty($jml_data) ? $this->qty_data : $jml_data);
                $start = ($page - 1) * $jml_data;
                $limit = $jml_data.','.$start;

                $order = "presensi_kegiatan.id_presensi_kegiatan DESC";
                $where = "presensi_kegiatan.active = 1  and kegiatan.active = 1 and kegiatan.id_kegiatan = '".$id_kegiatan."' and (presensi_kegiatan.id_presensi_kegiatan like '%".$filter."%' or presensi_kegiatan.id_kegiatan like '%".$filter."%' or presensi_kegiatan.id_jemaat like '%".$filter."%' or presensi_kegiatan.kode_jemaat like '%".$filter."%' or presensi_kegiatan.nama_jemaat like '%".$filter."%' or presensi_kegiatan.id_sektor like '%".$filter."%' or presensi_kegiatan.nama_sektor like '%".$filter."%' or presensi_kegiatan.jns_kelamin like '%".$filter."%' or presensi_kegiatan.tgl_lahir like '%".$filter."%' or presensi_kegiatan.id_pelkat like '%".$filter."%' or presensi_kegiatan.nama_pelkat like '%".$filter."%' or presensi_kegiatan.singkatan_pelkat like '%".$filter."%' or presensi_kegiatan.user_create like '%".$filter."%' or presensi_kegiatan.time_create like '%".$filter."%' or presensi_kegiatan.time_update like '%".$filter."%' or presensi_kegiatan.user_update like '%".$filter."%' )";
                $send_data = array('where' => $where, 'join' => $relation, 'limit' => $limit, 'order' => $order);
                $load_data = $this->presensi_kegiatan->load_data($send_data);
                $result = $load_data->result();

                #find last page...
                $select = "count(-1) jml";
                $send_data = array('where' => $where, 'join' => $relation, 'select' => $select);
                $load_data = $this->presensi_kegiatan->load_data($send_data);
                $total_data = $load_data->row()->jml;

                $last_page = ceil($total_data / $jml_data);
                $result = array('result' => $result, 'last_page' => $last_page);

                echo json_encode($result);
            }
        }

    }


    public function simpan_pilihan(){
        if($this->validasi_login()){
            $data_receive = json_decode(urldecode($this->input->post('data_send')));
            $token = $data_receive->token;
            $id_kegiatan = $data_receive->id_kegiatan;
            $id_jemaat = $data_receive->id_jemaat;

            $return = array();
            if($this->tokenStatus($token, 'SEND_DATA')){

                $where = array('id_presensi_kegiatan' => $id_presensi_kegiatan);
                $exe = $this->presensi_kegiatan->soft_delete($where);
                $return['sts'] = $exe;
            }

            echo json_encode($return);
        }
    }
    public function hapus(){
        if($this->validasi_login()){
            $data_receive = json_decode(urldecode($this->input->post('data_send')));
            $token = $data_receive->token;
            $id_presensi_kegiatan = $data_receive->id_presensi_kegiatan;

            $return = array();
            if($this->tokenStatus($token, 'SEND_DATA')){
                $where = array('id_presensi_kegiatan' => $id_presensi_kegiatan);
                $exe = $this->presensi_kegiatan->soft_delete($where);
                $return['sts'] = $exe;
            }

            echo json_encode($return);
        }
    }

}
