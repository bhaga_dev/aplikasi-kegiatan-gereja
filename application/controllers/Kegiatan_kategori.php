
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kegiatan_kategori extends MY_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model("Kegiatan_kategori_model","kegiatan_kategori");
	}

    public function load_data(){
        if($this->validasi_login()){
            $data_receive = json_decode(urldecode($this->input->post('data_send')));
            $token = $data_receive->token;
            if($this->tokenStatus($token, 'LOAD_DATA')){
                $filter = $data_receive->filter;


                $page = $data_receive->page;
                $jml_data = $data_receive->jml_data;

                $page = (empty($page) ? 1 : $page);
                $jml_data = (empty($jml_data) ? $this->qty_data : $jml_data);
                $start = ($page - 1) * $jml_data;
                $limit = $jml_data.','.$start;

                $order = "kegiatan_kategori.id_kegiatan_kategori DESC";
                $where = "kegiatan_kategori.active = 1  and (kegiatan_kategori.nama_kegiatan_kategori like '%".$filter."%')";
                $send_data = array('where' => $where, 'limit' => $limit, 'order' => $order);
                $load_data = $this->kegiatan_kategori->load_data($send_data);
                $result = $load_data->result();

                #find last page...
                $select = "count(-1) jml";
                $send_data = array('where' => $where, 'select' => $select);
                $load_data = $this->kegiatan_kategori->load_data($send_data);
                $total_data = $load_data->row()->jml;

                $last_page = ceil($total_data / $jml_data);
                $result = array('result' => $result, 'last_page' => $last_page);

                echo json_encode($result);
            }
        }

    }

    public function simpan(){
        if($this->validasi_login()){
            $token = $this->input->post('token');
            $return = array();
            if($this->tokenStatus($token, 'SEND_DATA')){
                $id_kegiatan_kategori = htmlentities($this->input->post('id_kegiatan_kategori'));
				$nama_kegiatan_kategori = htmlentities($this->input->post('nama_kegiatan_kategori'));
				$tipe_kegiatan = htmlentities($this->input->post('tipe_kegiatan'));
				$user_create = $this->session->userdata('id_admin');
				$time_create = date('Y-m-d H:i:s');
				$time_update = date('Y-m-d H:i:s');
				$user_update = $this->session->userdata('id_admin');

                $action = htmlentities($this->input->post('action'));

                #jika action memiliki value 'save' maka data akan disimpan.
                #jika action tidak memiliki value, maka akan dianggap sebagai upadate.
                if($action == 'save'){
                    $data = array('nama_kegiatan_kategori' => $nama_kegiatan_kategori,
								'tipe_kegiatan' => $tipe_kegiatan,
								'user_create' => $user_create,
								'time_create' => $time_create,
								'time_update' => $time_update,
								'user_update' => $user_update);
                        $exe = $this->kegiatan_kategori->save($data);
                        $return['sts'] = $exe;
                }
                else{
                    $data = array('nama_kegiatan_kategori' => $nama_kegiatan_kategori,
								'tipe_kegiatan' => $tipe_kegiatan,
								'time_update' => $time_update,
								'user_update' => $user_update);
                        $where = array('id_kegiatan_kategori' => $id_kegiatan_kategori);
                        $exe = $this->kegiatan_kategori->update($data, $where);
                        $return['sts'] = $exe;
                }
            }

            echo json_encode($return);
        }
    }
    public function hapus(){
        if($this->validasi_login()){
            $data_receive = json_decode(urldecode($this->input->post('data_send')));
            $token = $data_receive->token;
            $id_kegiatan_kategori = $data_receive->id_kegiatan_kategori;

            $return = array();
            if($this->tokenStatus($token, 'SEND_DATA')){
                $where = array('id_kegiatan_kategori' => $id_kegiatan_kategori);
                $exe = $this->kegiatan_kategori->soft_delete($where);
                $return['sts'] = $exe;

            }

            echo json_encode($return);
        }
    }

}
