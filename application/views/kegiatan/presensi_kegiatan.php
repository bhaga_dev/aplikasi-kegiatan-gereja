<!DOCTYPE html>
<html lang="en" >
<!-- begin::Head -->
<head>
    <?php $this->view('include/head'); ?>
    <?php $this->view('include/css'); ?>
</head>
<!-- end::Head -->
<!-- end::Body -->

<body class="kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--fixed kt-page--loading">

<!-- begin:: Page -->
<?php $this->view('include/header_mobile');?>

<div class="kt-grid kt-grid--hor kt-grid--root">
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
        <?php $this->view('include/left_side_navbar'); ?>
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
            <?php $this->view('include/top_navbar'); ?>
            <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

                <!-- begin:: Content Head -->
                <div class="kt-subheader  kt-grid__item" id="kt_subheader">
                    <div class="kt-container  kt-container--fluid ">
                        <div class="kt-subheader__main">
                            <h3 class="kt-subheader__title">
                                Presensi Kegiatan
                            </h3>
                        </div>
                        <div class="kt-subheader__toolbar">
                            <div class="kt-subheader__wrapper">

                            </div>
                        </div>
                    </div>
                </div>

                <!-- end:: Content Head -->

                <!-- begin:: Content -->
                <div class="row">
                    <div class="col-sm-12" id="kegiatan-box">
                        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                            <div class="kt-portlet kt-portlet--mobile">
                                <div class="kt-portlet__head kt-portlet__head--noborder">
                                    <div class="kt-portlet__head-label">
                                        <h3 class="kt-portlet__head-title">
                                            Kegiatan
                                        </h3>
                                    </div>
                                </div>
                                <div class="kt-portlet__body">
                                    <div id="pilih_kegiatan-box" class="kt-align-center" style="border: 1px dashed #eaeaea; border-radius: 6px; padding: 20px;">
                                        <div class="mb-4">
                                            <img src="<?php echo base_url(); ?>assets/images/calendar.svg" style="height: 80px;">
                                        </div>
                                        <button class="btn btn-brand waves-effect waves-light" type="button" id="pilih_kegiatan-btn">
                                            <span class="btn-label"><i class="la la-calendar-check-o"></i>
                                            </span>Pilih Kegiatan Disini
                                        </button>
                                    </div>
                                    <div id="detail_kegiatan-box" class="hidden">
                                        <h4 id="detail_kegiatan-nama_kegiatan"></h4>
                                        <div class="row mt-3 mb-2">
                                            <div class="col-3">Waktu</div>
                                            <div class="col-9 font-weight-bolder" id="detail_kegiatan-waktu"></div>
                                        </div>
                                        <div class="row mb-2">
                                            <div class="col-3">Tempat</div>
                                            <div class="col-9 font-weight-bolder" id="detail_kegiatan-tempat"></div>
                                        </div>
                                        <div class="row mb-2">
                                            <div class="col-3">Kategori</div>
                                            <div class="col-9 font-weight-bolder" id="detail_kegiatan-kegiatan_kategori"></div>
                                        </div>
                                        <div class="row mb-2">
                                            <div class="col-3">Tipe Kegiatan</div>
                                            <div class="col-9 font-weight-bolder" id="detail_kegiatan-tipe_kegiatan"></div>
                                        </div>
                                        <div style="border-bottom: 1px dashed;" class="mt-4"></div>
                                        <div class="mt-3">
                                            <input type="hidden" id="detail_kegiatan-id_kegiatan" name="id_kegiatan">
                                            <label>Jemaat</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control form-control-lg" id="detail_kegiatan-kode_jemaat" name="kode_jemaat">
                                                <div class="input-group-append">
                                                    <button class="btn btn-primary" id="detail_kegiatan-cari_jemaat-btn" type="button" onclick="cari_jemaat()">Cari</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="jemaat_banyak-box" class="hidden">
                                            <div style="border-bottom: 1px dashed;" class="mt-4"></div>
                                            <div class="table-responsive">
                                                <table class="table table-sm table-striped" id="jemaat_banyak">
                                                    <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>Nama Jemaat</th>
                                                        <th>Sektor</th>
                                                        <th>Action</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 hidden" id="data_jemaat-box">
                        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                            <div class="kt-portlet kt-portlet--mobile">
                                <div class="kt-portlet__head kt-portlet__head--noborder">
                                    <div class="kt-portlet__head-label">
                                        <h3 class="kt-portlet__head-title">
                                            Data Jemaat
                                        </h3>
                                    </div>
                                </div>
                                <div class="kt-portlet__body">
                                    <div id="data_zona_presensi_kegiatan">
                                        <div class="kt-input-icon kt-input-icon--left mb-3">
                                            <input type="text" autocomplete="off" class="form-control form-control-filter" id="filter" name="filter" placeholder="Masukkan kata kunci pencarian">
                                            <span class="kt-input-icon__icon kt-input-icon__icon--left">
                                                <span><i class="la la-search"></i></span>
                                            </span>
                                        </div>
                                        <div class="table-responsive">
                                            <table class="table table-sm table-striped" id="data_presensi_kegiatan">
                                                <thead>
                                                <tr>
                                                    <th>No.</th>
                                                    <th>Nama Jemaat</th>
                                                    <th>Sektor</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="kt-datatable kt-datatable--default">
                                            <div class="kt-datatable__pager kt-datatable--paging-loaded">
                                                <input type="hidden" id="page" name="page" value="1">
                                                <input type="hidden" id="last_page" name="last_page">
                                                <input type="hidden" id="last_page_status" name="last_page_status" value="false">
                                                <ul class="kt-datatable__pager-nav">
                                                    <li>
                                                        <a title="First" class="kt-datatable__pager-link kt-datatable__pager-link--first" onclick="page('first')">
                                                            <i class="flaticon2-fast-back"></i>
                                                        </a>
                                                    </li>
                                                    <li id="info_halaman">
                                                        <a title="Previous" class="kt-datatable__pager-link kt-datatable__pager-link--prev" onclick="page('previous')">
                                                            <i class="flaticon2-back"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a title="Next" class="kt-datatable__pager-link kt-datatable__pager-link--next" onclick="page('next')">
                                                            <i class="flaticon2-next"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a title="Last" class="kt-datatable__pager-link kt-datatable__pager-link--last" onclick="page('last')">
                                                            <i class="flaticon2-fast-next"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end:: Content -->
            </div>

            <?php $this->view('include/footer'); ?>
        </div>
    </div>
</div>

<?php $this->view('include/js'); ?>
<div class="modal fade" id="list_kegiatan_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    List Kegiatan Hari Ini
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div id="data_zona_kegiatan">
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="kt-input-icon kt-input-icon--left mb-3">
                                <input type="text" autocomplete="off" class="form-control form-control-filter" id="filter_kegiatan" name="filter_kegiatan" placeholder="Masukkan kata kunci pencarian">
                                <span class="kt-input-icon__icon kt-input-icon__icon--left">
                                    <span><i class="la la-search"></i></span>
                                </span>
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <div class="input-group">
                                <input type="text" class="form-control datepicker" id="periode_awal_kegiatan" name="periode_awal_kegiatan" placeholder="dd-mm-yyyy" readonly>
                                <div class="input-group-append">
                                    <span class="input-group-text">Sampai</span>
                                </div>
                                <input type="text" class="form-control datepicker" id="periode_akhir_kegiatan" name="periode_akhir_kegiatan" placeholder="dd-mm-yyyy" readonly>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <button class="btn btn-primary m-btn m-btn--custom btn-block" id="tampilkan_kegiatan">
                                Cari
                            </button>
                        </div>
                    </div>


                    <div class="table-responsive">
                        <table class="table table-sm table-striped" id="data_kegiatan" style="white-space: unset;">
                            <thead>
                            <tr>
                                <th style="width: 7%">No.</th>
                                <th style="width: 40%">Nama Kegiatan</th>
                                <th style="width: 40%">Tempat Kegiatan</th>
                                <th style="width: 13%">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <div class="kt-datatable kt-datatable--default">
                        <div class="kt-datatable__pager kt-datatable--paging-loaded">
                            <input type="hidden" id="page_kegiatan" name="page_kegiatan" value="1">
                            <input type="hidden" id="last_page_kegiatan" name="last_page_kegiatan">
                            <input type="hidden" id="last_page_status_kegiatan" name="last_page_status_kegiatan" value="false">
                            <ul class="kt-datatable__pager-nav">
                                <li>
                                    <a title="First" class="kt-datatable__pager-link kt-datatable__pager-link--first" onclick="page_kegiatan('first')">
                                        <i class="flaticon2-fast-back"></i>
                                    </a>
                                </li>
                                <li id="info_halaman_kegiatan">
                                    <a title="Previous" class="kt-datatable__pager-link kt-datatable__pager-link--prev" onclick="page_kegiatan('previous')">
                                        <i class="flaticon2-back"></i>
                                    </a>
                                </li>
                                <li>
                                    <a title="Next" class="kt-datatable__pager-link kt-datatable__pager-link--next" onclick="page_kegiatan('next')">
                                        <i class="flaticon2-next"></i>
                                    </a>
                                </li>
                                <li>
                                    <a title="Last" class="kt-datatable__pager-link kt-datatable__pager-link--last" onclick="page_kegiatan('last')">
                                        <i class="flaticon2-fast-next"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
<script>
    $("#pilih_kegiatan-btn").click(function(){
        $("#list_kegiatan_modal").modal('show');
        initDatepicker();
    });

    function page_kegiatan(tipe){
        if(tipe == 'first'){
            $("#last_page_status_kegiatan").val('false');
            $("#page_kegiatan").val(1);
            load_data_kegiatan();
        }
        else if(tipe == 'previous'){
            var page = parseInt($("#page_kegiatan").val());
            var previous_page = page - 1;
            previous_page = (previous_page <= 1 ? 1 : previous_page);
            if(page != previous_page){
                $("#last_page_status_kegiatan").val('false');
                $("#page_kegiatan").val(previous_page);
                load_data_kegiatan();
            }
        }
        else if(tipe == 'next'){
            var last_page_status = $("#last_page_status_kegiatan").val();
            var page = parseInt($("#page_kegiatan").val());
            var next_page = page + 1;
            if(last_page_status == "false"){
                $("#page_kegiatan").val(next_page);
                load_data_kegiatan();
            }
        }
        else if(tipe == 'last'){
            var last_page = parseInt($("#last_page_kegiatan").val());
            var page = parseInt($("#page_kegiatan").val());
            if(last_page != page){
                $("#page_kegiatan").val(last_page);
                load_data_kegiatan();
            }
        }
        else{
            $("#page_kegiatan").val(tipe);
            load_data_kegiatan();
        }
    }

    $("#filter_kegiatan").keyup(function(){
        $("#last_page_status_kegiatan").val('false');
        $("#page_kegiatan").val(1);
        ajax_request_kegiatan.abort();
        load_data_kegiatan();
    });
    $("#tampilkan_kegiatan").click(function(){
        $("#last_page_status_kegiatan").val('false');
        $("#page_kegiatan").val(1);
        ajax_request_kegiatan.abort();
        load_data_kegiatan();
    });

    var ajax_request_kegiatan;
    var kegiatan;
    function load_data_kegiatan(){
        var page = $("#page_kegiatan").val();
        var jml_data = 10;

        var filter = $("#filter_kegiatan").val();
        var periode_awal = $("#periode_awal_kegiatan").val();
        var periode_akhir = $("#periode_akhir_kegiatan").val();

        var data = new Object;
        data['token'] = '<?php echo genToken('LOAD_DATA'); ?>';
        data['page'] = page;
        data['jml_data'] = jml_data;
        data['filter'] = filter;
        data['periode_awal'] = periode_awal;
        data['periode_akhir'] = periode_akhir;

        elementLoading('show', '#data_kegiatan');
        ajax_request_kegiatan = $.ajax({
            type: "POST",
            url: '<?php echo base_url(); ?>Kegiatan/load_data',
            data: 'data_send='+encodeURIComponent(JSON.stringify(data)),
            cache: false,
            dataType: "json",
            success: function(result){
                elementLoading('hide', '#data_kegiatan');

                $("#last_page_kegiatan").val(result.last_page);
                kegiatan = result.result;
                info_halaman('#info_halaman_kegiatan', page, result.last_page);

                var rangkai = '';
                if(kegiatan.length > 0){
                    for(var i=0; i < kegiatan.length; i++){
                        if(reformatDate(kegiatan[i].waktu_mulai, false, 'YYYY-MM-DD') == reformatDate(kegiatan[i].waktu_selesai, false, 'YYYY-MM-DD'))
                            var waktu = reformatDate(kegiatan[i].waktu_mulai, false, 'DD MMM YYYY HH:mm')+' - '+reformatDate(kegiatan[i].waktu_selesai, false, 'HH:mm');
                        else
                            var waktu = reformatDate(kegiatan[i].waktu_mulai, 'DD MMM YYYY HH:mm')+' - '+reformatDate(kegiatan[i].waktu_selesai, 'DD MMM YYYY HH:mm');

                        rangkai += '<tr>' +
                            '<td>'+(((page - 1) * jml_data) + i+1)+'.</td>' +
                            '<td>'+coverMe(kegiatan[i].nama_kegiatan)+'<div class="kt-font-sm mb-2">'+waktu+'</div></td>' +
                            '<td>'+coverMe(kegiatan[i].tempat_kegiatan)+'</td>'+
                            '<td>'+
                                '<button type="button" class="btn btn-sm btn-primary" onClick="pilih_kegiatan('+i+')" title="Edit data">Pilih</button>'+
                            '</td>' +
                            '</tr>';
                    }
                }

                if(kegiatan.length < jml_data)
                    $("#last_page_status_kegiatan").val('true');


                if(rangkai){
                    $("#data_kegiatan tbody").html(rangkai);
                }
                else{
                    rangkai = '<tr>' +
                        '<td colspan="4" style="text-align: center">Tidak ada kegiatan yang anda cari</td>' +
                        '</tr>';
                    $("#data_kegiatan tbody").html(rangkai);
                }

            }

        });
    }
    load_data_kegiatan();
    function pilih_kegiatan(index){
        var data = kegiatan[index];

        if(reformatDate(data.waktu_mulai, false, 'YYYY-MM-DD') == reformatDate(data.waktu_selesai, false, 'YYYY-MM-DD'))
            var waktu = reformatDate(data.waktu_mulai, false, 'DD MMM YYYY HH:mm')+' - '+reformatDate(data.waktu_selesai, false, 'HH:mm');
        else
            var waktu = reformatDate(data.waktu_mulai, 'DD MMM YYYY HH:mm')+' - '+reformatDate(data.waktu_selesai, 'DD MMM YYYY HH:mm');

        $("#detail_kegiatan-id_kegiatan").val(data.id_kegiatan);
        $("#detail_kegiatan-nama_kegiatan").html(data.nama_kegiatan);
        $("#detail_kegiatan-waktu").html(waktu);
        $("#detail_kegiatan-tempat").html(data.tempat_kegiatan);
        $("#detail_kegiatan-kegiatan_kategori").html(data.nama_kegiatan_kategori);
        $("#detail_kegiatan-tipe_kegiatan").html(data.tipe_kegiatan);

        $("#pilih_kegiatan-box").hide();
        $("#list_kegiatan_modal").modal('hide');
        $("#detail_kegiatan-box").show();
        load_data();
        $("#data_jemaat-box").show();
        $("#kegiatan-box").removeClass();
        $("#kegiatan-box").addClass('col-sm-6');

        $("#detail_kegiatan-kode_jemaat").focus();

    }
    $('#detail_kegiatan-kode_jemaat').keypress(function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
            cari_jemaat();
        }
    });
    function cari_jemaat(){
        $("#jemaat_banyak-box").hide();
        var kode_jemaat = $("#detail_kegiatan-kode_jemaat").val();
        var id_kegiatan = $("#detail_kegiatan-id_kegiatan").val();
        if(kode_jemaat){
            $("#detail_kegiatan-cari_jemaat-btn, #detail_kegiatan-kode_jemaat").attr('disabled', 'disabled');
            $("#detail_kegiatan-cari_jemaat-btn").html('Loading...');
            var data = new Object;
            data['token'] = '<?php echo genToken('LOAD_DATA'); ?>';
            data['kode_jemaat'] = kode_jemaat;
            data['id_kegiatan'] = id_kegiatan;
            data['mode'] = 'cari';

            $.ajax({
                type: "POST",
                url: '<?php echo base_url(); ?>presensi_kegiatan/cari_jemaat',
                data: 'data_send='+encodeURIComponent(JSON.stringify(data)),
                cache: false,
                dataType: "json",
                success: function(data){
                    $("#detail_kegiatan-cari_jemaat-btn, #detail_kegiatan-kode_jemaat").removeAttr('disabled');
                    $("#detail_kegiatan-cari_jemaat-btn").html('Cari');

                    if(data.sts == 1){
                        //load data..
                        <?php echo alert('simpan_berhasil'); ?>
                        load_data();
                        $("#detail_kegiatan-kode_jemaat").val('');
                        $("#detail_kegiatan-kode_jemaat").focus();
                    }
                    else if(data.sts == 'jemaat_banyak'){
                        var data_banyak = data.data;
                        var rangkai = '';
                        for(var i=0; i < data_banyak.length; i++){
                            rangkai += '<tr>' +
                                   '<td>'+(i+1)+'.</td>' +
                                    '<td>' +
                                        '<div class="kt-font-sm">'+coverMe(data_banyak[i].kode_jemaat)+'</div>' +
                                        coverMe(data_banyak[i].nama_jemaat)+
                                    '</td>' +
                                    '<td>'+coverMe(data_banyak[i].nama_sektor)+'</td>' +
                                    '<td>'+
                                        '<button type="button" class="btn btn-sm btn-brand" onClick="pilih_jemaat('+data_banyak[i].id_jemaat+')">Pilih</button>'+
                                    '</td>' +
                                '</tr>';
                        }
                        $("#jemaat_banyak tbody").html(rangkai);
                        $("#jemaat_banyak-box").show(200);
                    }
                    else if(data.sts == 'tidak_ada_jemaat'){
                        var pertanyaan = "Data jemaat tidak ada. Apakah anda ingin memasukkan sebagai tamu?";

                        konfirmasi(pertanyaan, function(){
                            input_sebagai_tamu();
                        });
                    }
                    else if(data.sts == 'duplikat_presensi'){
                        swal.fire("Perhatian!", "Jemaat tidak bisa didaftarkan ulang karena sudah terdaftar dalam kegiatan ini", "warning");
                    }
                    else if(data.sts == 'Token kadaluarsa'){
                        <?php echo alert('token_api_kadaluarsa'); ?>
                    }
                    else if(data.sts == 'Token bermasalah'){
                        <?php echo alert('token_api_bermasalah'); ?>
                    }
                    else{
                        <?php echo alert('proses_gagal'); ?>
                    }
                }
            })
        }
    }
    function pilih_jemaat(id_jemaat){
        //show loading animation...
        preloader('show');

        var id_kegiatan = $("#detail_kegiatan-id_kegiatan").val();
        var data = new Object;
        data['token'] = '<?php echo genToken('LOAD_DATA'); ?>';
        data['kode_jemaat'] = id_jemaat;
        data['id_kegiatan'] = id_kegiatan;
        data['mode'] = 'pilihan';

        $.ajax({
            type: "POST",
            url: '<?php echo base_url(); ?>Presensi_kegiatan/cari_jemaat',
            data: 'data_send='+encodeURIComponent(JSON.stringify(data)),
            cache: false,
            dataType: "text",
            success: function(msg){
                //hide loading animation...
                preloader('hide');
                //parse JSON...
                var data = safelyParseJSON(msg);

                if(data.sts == 1){
                    //load data..
                    load_data();
                    <?php echo alert('simpan_berhasil'); ?>
                    $("#jemaat_banyak-box").hide();
                    $("#detail_kegiatan-kode_jemaat").val('');
                    $("#detail_kegiatan-kode_jemaat").focus();
                }
                else if(data.sts == 'duplikat_presensi'){
                    swal.fire("Perhatian!", "Jemaat tidak bisa didaftarkan ulang karena sudah terdaftar dalam kegiatan ini", "warning");
                }
                else if(data.sts == 'tidak_ada_jemaat'){
                    var pertanyaan = "Data jemaat tidak ada. Apakah anda ingin memasukkan sebagai tamu?";

                    konfirmasi(pertanyaan, function(){
                        input_sebagai_tamu();
                    });
                }
                else if(data.sts == 'Token kadaluarsa'){
                    <?php echo alert('token_api_kadaluarsa'); ?>
                }
                else if(data.sts == 'Token bermasalah'){
                    <?php echo alert('token_api_bermasalah'); ?>
                }
                else{
                    <?php echo alert('proses_gagal'); ?>
                }
            }
        });
    }

    function input_sebagai_tamu(){
        preloader('show');

        var id_kegiatan = $("#detail_kegiatan-id_kegiatan").val();
        var kode_jemaat = $("#detail_kegiatan-kode_jemaat").val();
        var data = new Object;
        data['token'] = '<?php echo genToken('LOAD_DATA'); ?>';
        data['kode_jemaat'] = kode_jemaat;
        data['id_kegiatan'] = id_kegiatan;

        $.ajax({
            type: "POST",
            url: '<?php echo base_url(); ?>Presensi_kegiatan/simpan_tamu',
            data: 'data_send='+encodeURIComponent(JSON.stringify(data)),
            cache: false,
            dataType: "text",
            success: function(msg){
                //hide loading animation...
                preloader('hide');
                //parse JSON...
                var data = safelyParseJSON(msg);

                if(data.sts == 1){
                    //load data..
                    load_data();
                    <?php echo alert('simpan_berhasil'); ?>
                    $("#jemaat_banyak-box").hide();
                    $("#detail_kegiatan-kode_jemaat").val('');
                    $("#detail_kegiatan-kode_jemaat").focus();
                }
                else if(data.sts == 'duplikat_presensi'){
                    swal.fire("Perhatian!", "Jemaat tidak bisa didaftarkan ulang karena sudah terdaftar dalam kegiatan ini", "warning");
                }
                else if(data.sts == 'tidak ada jemaat'){
                    var pertanyaan = "Data jemaat tidak ada. Apakah anda ingin memasukkan sebagai tamu?";

                    konfirmasi(pertanyaan, function(){
                        input_sebagai_tamu();
                    });
                }
                else if(data.sts == 'Token kadaluarsa'){
                    <?php echo alert('token_api_kadaluarsa'); ?>
                }
                else if(data.sts == 'Token bermasalah'){
                    <?php echo alert('token_api_bermasalah'); ?>
                }
                else{
                    <?php echo alert('proses_gagal'); ?>
                }
            }
        });
    }

    var list_data;
    function hapus(index){
        var data = list_data[index];
        var pertanyaan = "Apakah anda yakin ingin menghapus data "+data.nama_jemaat+"?";

        konfirmasi(pertanyaan, function(){
            proses_hapus(data.id_presensi_kegiatan);
        });
    }
    function proses_hapus(id){
        //show loading animation...
        preloader('show');

        var data = new Object;
        data['token'] = '<?php echo genToken('SEND_DATA'); ?>';
        data['id_presensi_kegiatan'] = id;

        $.ajax({
            type: "POST",
            url: '<?php echo base_url(); ?>Presensi_kegiatan/hapus',
            data: 'data_send='+encodeURIComponent(JSON.stringify(data)),
            cache: false,
            dataType: "text",
            success: function(msg){
                //hide loading animation...
                preloader('hide');
                //parse JSON...
                var data = safelyParseJSON(msg);

                if(data.sts == 1){
                    //load data..
                    load_data();
                    <?php echo alert('hapus_berhasil'); ?>
                }
                else{
                    <?php echo alert('proses_gagal'); ?>
                }
            }

        });
    }

    function page(tipe){
        if(tipe == 'first'){
            $("#last_page_status").val('false');
            $("#page").val(1);
            load_data();
        }
        else if(tipe == 'previous'){
            var page = parseInt($("#page").val());
            var previous_page = page - 1;
            previous_page = (previous_page <= 1 ? 1 : previous_page);
            if(page != previous_page){
                $("#last_page_status").val('false');
                $("#page").val(previous_page);
                load_data();
            }
        }
        else if(tipe == 'next'){
            var last_page_status = $("#last_page_status").val();
            var page = parseInt($("#page").val());
            var next_page = page + 1;
            if(last_page_status == "false"){
                $("#page").val(next_page);
                load_data();
            }
        }
        else if(tipe == 'last'){
            var last_page = parseInt($("#last_page").val());
            var page = parseInt($("#page").val());
            if(last_page != page){
                $("#page").val(last_page);
                load_data();
            }
        }
        else{
            $("#page").val(tipe);
            load_data();

        }
    }

    $("#filter").keyup(function(){
        $("#last_page_status").val('false');
        $("#page").val(1);
        ajax_request.abort();
        load_data();
    });

    var ajax_request;
    function load_data(){
        var page = $("#page").val();
        var jml_data = 10;

        var filter = $("#filter").val();
        var id_kegiatan = $("#detail_kegiatan-id_kegiatan").val();

        var data = new Object;
        data['token'] = '<?php echo genToken('LOAD_DATA'); ?>';
        data['page'] = page;
        data['jml_data'] = jml_data;
        data['filter'] = filter;
        data['id_kegiatan'] = id_kegiatan;

        elementLoading('show', '#data_presensi_kegiatan');
        ajax_request = $.ajax({
            type: "POST",
            url: '<?php echo base_url(); ?>Presensi_kegiatan/load_data',
            data: 'data_send='+encodeURIComponent(JSON.stringify(data)),
            cache: false,
            dataType: "text",
            success: function(msg){
                elementLoading('hide', '#data_presensi_kegiatan');
                //parse JSON...
                var result = safelyParseJSON(msg);
                    $("#last_page").val(result.last_page);
                    list_data = result.result;
                    info_halaman('#info_halaman', page, result.last_page);

                var rangkai = '';
                if(list_data.length > 0){
                    for(var i=0; i < list_data.length; i++){
                        rangkai += '<tr>' +
                                        '<td>'+(((page - 1) * jml_data) + i+1)+'.</td>' +
                                        '<td>' +
                                            '<div class="kt-font-sm">'+coverMe(list_data[i].kode_jemaat)+'</div>' +
                                            coverMe(list_data[i].nama_jemaat)+
                                        '</td>' +
                                        '<td>'+coverMe(list_data[i].nama_sektor)+'</td>' +
                                        '<td>'+
                                            '<button type="button" class="btn btn-sm btn-clean btn-icon btn-icon-md" onClick="hapus('+i+')" title="Hapus data"><i class="la la-trash"></i></button>'+
                                        '</td>' +
                                    '</tr>';
                    }
                }

                if(list_data.length < jml_data)
                        $("#last_page_status").val('true');


                if(rangkai){
                    $("#empty_state").remove();
                    $("#data_zona_presensi_kegiatan").show();

                    $("#data_presensi_kegiatan tbody").html(rangkai);
                }
                else{
                    if(page == 1 && filter == ''){
                        var element = "#data_zona_presensi_kegiatan";
                        var html = '<div style="text-align: center" id="empty_state"><img src="'+base_url+'assets/images/empty_chair.webp" width="200px">' +
                            '<h5>Belum ada jemaat.</h5></div>';
                        $(element).hide();
                        $(element).after(html);
                    }
                    else
                        $("#data_presensi_kegiatan tbody").html('');
                }

            }

        });
    }
</script>
</body>
</html>
