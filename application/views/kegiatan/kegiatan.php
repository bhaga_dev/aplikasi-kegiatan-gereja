<!DOCTYPE html>
<html lang="en" >
<!-- begin::Head -->
<head>
    <?php $this->view('include/head'); ?>
    <?php $this->view('include/css'); ?>
</head>
<!-- end::Head -->
<!-- end::Body -->

<body class="kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--fixed kt-page--loading">

<!-- begin:: Page -->
<?php $this->view('include/header_mobile');?>

<div class="kt-grid kt-grid--hor kt-grid--root">
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
        <?php $this->view('include/left_side_navbar'); ?>
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
            <?php $this->view('include/top_navbar'); ?>
            <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

                <!-- begin:: Content Head -->
                <div class="kt-subheader  kt-grid__item" id="kt_subheader">
                    <div class="kt-container  kt-container--fluid ">
                        <div class="kt-subheader__main">
                            <h3 class="kt-subheader__title">
                                Kegiatan
                            </h3>
                        </div>
                        <div class="kt-subheader__toolbar">
                            <div class="kt-subheader__wrapper">
                                <button class="btn btn-primary m-btn m-btn--custom m-btn--icon tabel_zone" onclick="form_action('show')">
                                    <span>
                                        <i class="la la-plus"></i>
                                        <span>
                                            Tambah Kegiatan
                                        </span>
                                    </span>
                                </button>
                                <button class="btn btn-default m-btn m-btn--custom m-btn--icon form_zone" onclick="form_action('hide')" style="display:none;">
                                    <span>
                                        <i class="la la-arrow-left"></i>
                                        <span>
                                            Kembali
                                        </span>
                                    </span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- end:: Content Head -->

                <!-- begin:: Content -->
                <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid form_zone hidden" id="form_kegiatan">
                    <div class="kt-portlet kt-portlet--mobile">
                        <div class="kt-portlet__head kt-portlet__head--noborder">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Form Kegiatan
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">
                            <form id="input_form_kegiatan" class="kt-form kt-form--label-right" action="<?php echo base_url(); ?>Kegiatan/simpan" method="post" autocomplete="off">
                                <input type="hidden" class="form-control" id="id_kegiatan" name="id_kegiatan" maxlength="11" placeholder="">
                                <div class="form-group row">
                                    <label class="<?php echo $kolom_label; ?> col-form-label">Nama Kegiatan <?php echo $red_star; ?></label>
                                    <div class="col-sm-9">
                                        <input type="text" autocomplate="off" class="form-control" id="nama_kegiatan" name="nama_kegiatan" maxlength="200" placeholder="" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="<?php echo $kolom_label; ?> col-form-label">Tempat Kegiatan </label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="tempat_kegiatan" name="tempat_kegiatan" maxlength="200">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="<?php echo $kolom_label; ?> col-form-label">Waktu Mulai <?php echo $red_star; ?></label>
                                    <div class="col-sm-3">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">
                                                    <i class="la la-clock-o"></i>
                                                </span>
                                            </div>
                                             <input type="text" autocomplate="off" class="form-control datetimepicker" id="waktu_mulai" name="waktu_mulai" placeholder="" readonly style="background-color: #FFF" required>
                                        </div>
                                    </div>

                                    <label class="<?php echo $kolom_label; ?> col-form-label">Waktu Selesai <?php echo $red_star; ?></label>
                                    <div class="col-sm-3">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">
                                                    <i class="la la-clock-o"></i>
                                                </span>
                                            </div>
                                             <input type="text" autocomplate="off" class="form-control datetimepicker" id="waktu_selesai" name="waktu_selesai" placeholder="" readonly style="background-color: #FFF" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="<?php echo $kolom_label; ?> col-form-label">Kategori Kegiatan <?php echo $red_star; ?></label>
                                    <div class="col-sm-3">
                                        <select class="form-control select2" id="id_kegiatan_kategori" name="id_kegiatan_kategori" required>
                                        <?php
                                        if($konten['kegiatan_kategori']->num_rows() > 0){
                                            foreach($konten['kegiatan_kategori']->result() as $list){
                                                echo '<option value="'.$list->id_kegiatan_kategori.'">'.$list->nama_kegiatan_kategori.'</option>';
                                            }
                                        }
                                        ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="<?php echo $kolom_label; ?> col-form-label">Kegiatan Berulang? <?php echo $red_star; ?></label>
                                    <div class="col-sm-3">
                                        <select class="form-control select2" id="tipe_berulang" name="tipe_berulang" required>
                                            <option value="Tidak" selected>Tidak</option>
                                            <option value="Setiap Hari" selected>Setiap Hari</option>
                                            <option value="Setiap Minggu">Setiap Minggu</option>
                                            <option value="Setiap Bulan">Setiap Bulan</option>
                                            <option value="Setiap Tahun">Setiap Tahun</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="<?php echo $kolom_label; ?>"></div>
                                    <div class="col-sm-9">
                                        <button class="btn btn-success waves-effect waves-light" type="submit" id="simpan">
                                            <span class="btn-label"><i class="la la-save"></i>
                                            </span>Simpan
                                        </button>
                                        <button class="btn btn-secondary waves-effect waves-light" type="button" onclick="form_action('hide')">
                                            <span class="btn-label"><i class="la la-times"></i>
                                            </span>Batal
                                        </button>
                                        <input type="hidden" id="action" name="action" value="save">
                                        <input type="hidden" id="token" name="token" value="<?php echo genToken('SEND_DATA'); ?>">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid tabel_zone" id="tabel_kegiatan">
                    <div class="kt-portlet kt-portlet--mobile">
                        <div class="kt-portlet__head kt-portlet__head--noborder">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Data Kegiatan
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">
                            <div id="data_zona_kegiatan">
                                <div class="row">
                                    <div class="col-sm-5">
                                        <div class="kt-input-icon kt-input-icon--left mb-3">
                                            <input type="text" autocomplete="off" class="form-control form-control-filter" id="filter" name="filter" placeholder="Masukkan kata kunci pencarian">
                                            <span class="kt-input-icon__icon kt-input-icon__icon--left">
                                            <span><i class="la la-search"></i></span></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="input-group">
                                            <input type="text" class="form-control datepicker" id="periode_awal" name="periode_awal" placeholder="dd-mm-yyyy" readonly>
                                            <div class="input-group-append">
                                                <span class="input-group-text">Sampai</span>
                                            </div>
                                            <input type="text" class="form-control datepicker" id="periode_akhir" name="periode_akhir" placeholder="dd-mm-yyyy" readonly>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <button class="btn btn-primary m-btn m-btn--custom btn-block" id="tampilkan">
                                            Cari
                                        </button>
                                    </div>
                                </div>


                                <div class="table-responsive">
                                    <table class="table table-sm table-striped" id="data_kegiatan" style="white-space: unset;">
                                        <thead>
                                        <tr>
                                            <th style="width: 7%">No.</th>
                                            <th style="width: 20%">Nama Kegiatan</th>
                                            <th style="width: 20%">Tempat Kegiatan</th>
                                            <th style="width: 20%">Kategori Kegiatan</th>
                                            <th style="width: 10%">Tipe Kegiatan</th>
                                            <th style="width: 13%">Berulang?</th>
                                            <th style="width: 10%">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="kt-datatable kt-datatable--default">
                                    <div class="kt-datatable__pager kt-datatable--paging-loaded">
                                        <input type="hidden" id="page" name="page" value="1">
                                        <input type="hidden" id="last_page" name="last_page">
                                        <input type="hidden" id="last_page_status" name="last_page_status" value="false">
                                        <ul class="kt-datatable__pager-nav">
                                            <li>
                                                <a title="First" class="kt-datatable__pager-link kt-datatable__pager-link--first" onclick="page('first')">
                                                    <i class="flaticon2-fast-back"></i>
                                                </a>
                                            </li>
                                            <li id="info_halaman">
                                                <a title="Previous" class="kt-datatable__pager-link kt-datatable__pager-link--prev" onclick="page('previous')">
                                                    <i class="flaticon2-back"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a title="Next" class="kt-datatable__pager-link kt-datatable__pager-link--next" onclick="page('next')">
                                                    <i class="flaticon2-next"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a title="Last" class="kt-datatable__pager-link kt-datatable__pager-link--last" onclick="page('last')">
                                                    <i class="flaticon2-fast-next"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- end:: Content -->
            </div>

            <?php $this->view('include/footer'); ?>
        </div>
    </div>
</div>

<?php $this->view('include/js'); ?>

<script>
    function form_action(action){
        //reset form...
        $("#input_form_kegiatan")[0].reset();
        $("#action").val('save');
        $(".select2").val('').trigger('change');
        $("#tipe_berulang").removeAttr('disabled');

        if(action == 'show'){
            $(".form_zone").fadeIn(300);
            $(".tabel_zone").hide();
        }
        else{
            $(".form_zone").hide();
            $(".tabel_zone").fadeIn(300);
        }
    }

    var list_data;
    $("#input_form_kegiatan").on('submit', function(e){
        e.preventDefault();

        var id_kegiatan = $("#id_kegiatan").val();
        var nama_kegiatan = $("#nama_kegiatan").val();
        var waktu_mulai = $("#waktu_mulai").val();
        var waktu_selesai = $("#waktu_selesai").val();
        var tempat_kegiatan = $("#tempat_kegiatan").val();
        var id_kegiatan_kategori = $("#id_kegiatan_kategori").val();

        var action = $("#action").val();

        if(!action  || !nama_kegiatan || !waktu_mulai || !waktu_selesai || !id_kegiatan_kategori){
            <?php echo alert('kosong'); ?>
        }
        else if(!isDate(waktu_mulai, true)){
            <?php echo alert('format_tgl_salah'); ?>
        }else if(!isDate(waktu_selesai, true)){
            <?php echo alert('format_tgl_salah'); ?>
        }
        else{
            preloader('show');
            jQuery(this).ajaxSubmit({
                success:  function(msg){
                    var data = safelyParseJSON(msg);
                    preloader('hide');

                    if(data.sts == 1){
                        //hapus seluruh field...
                        $("#last_page_status").val('false');
                        $("#page").val(1);

                        $("#input_form_kegiatan")[0].reset();
                        $("#action").val('save');
                        form_action('hide');
                        //load data..
                        load_data();
                        <?php echo alert('simpan_berhasil'); ?>
                    }
                    else{
                        <?php echo alert('proses_gagal'); ?>
                    }
                }
            });
        }
    });

    function edit(index){
        form_action('show');
        var data = list_data[index];
        $("#id_kegiatan").val(decodeHtml(data.id_kegiatan));
        $("#nama_kegiatan").val(decodeHtml(data.nama_kegiatan));
        $("#waktu_mulai").val(reformatDate(data.waktu_mulai, false, 'DD-MM-YYYY HH:mm'));
        $("#waktu_selesai").val(reformatDate(data.waktu_selesai, false, 'DD-MM-YYYY HH:mm'));
        $("#tempat_kegiatan").val(decodeHtml(data.tempat_kegiatan));
        $("#id_kegiatan_kategori").val(decodeHtml(data.id_kegiatan_kategori)).trigger('change');
        $("#tipe_berulang").val(decodeHtml(data.tipe_berulang)).trigger('change');
        $("#tipe_berulang").attr('disabled', 'disabled');

        $("#action").val('update');

    }
    function hapus(index, tipe_hapus){
        var data = list_data[index];
        var pertanyaan = "Apakah anda yakin ingin menghapus data "+data.nama_kegiatan+"?";

        konfirmasi(pertanyaan, function(){
            proses_hapus(data.id_kegiatan, tipe_hapus);
        });
    }
    function proses_hapus(id, tipe_hapus){
        //show loading animation...
        preloader('show');

        var data = new Object;
        data['token'] = '<?php echo genToken('SEND_DATA'); ?>';
        data['id_kegiatan'] = id;
        data['tipe_hapus'] = tipe_hapus;

        $.ajax({
            type: "POST",
            url: '<?php echo base_url(); ?>Kegiatan/hapus',
            data: 'data_send='+encodeURIComponent(JSON.stringify(data)),
            cache: false,
            dataType: "text",
            success: function(msg){
                //hide loading animation...
                preloader('hide');
                //parse JSON...
                var data = safelyParseJSON(msg);

                if(data.sts == 1){
                    //load data..
                    load_data();
                    <?php echo alert('hapus_berhasil'); ?>
                }
                else{
                    <?php echo alert('proses_gagal'); ?>
                }
            }

        });
    }

    function page(tipe){
        if(tipe == 'first'){
            $("#last_page_status").val('false');
            $("#page").val(1);
            load_data();
        }
        else if(tipe == 'previous'){
            var page = parseInt($("#page").val());
            var previous_page = page - 1;
            previous_page = (previous_page <= 1 ? 1 : previous_page);
            if(page != previous_page){
                $("#last_page_status").val('false');
                $("#page").val(previous_page);
                load_data();
            }
        }
        else if(tipe == 'next'){
            var last_page_status = $("#last_page_status").val();
            var page = parseInt($("#page").val());
            var next_page = page + 1;
            if(last_page_status == "false"){
                $("#page").val(next_page);
                load_data();
            }
        }
        else if(tipe == 'last'){
            var last_page = parseInt($("#last_page").val());
            var page = parseInt($("#page").val());
            if(last_page != page){
                $("#page").val(last_page);
                load_data();
            }
        }
        else{
            $("#page").val(tipe);
            load_data();

        }
    }

    $("#filter").keyup(function(){
        $("#last_page_status").val('false');
        $("#page").val(1);
        ajax_request.abort();
        load_data();
    });
    $("#tampilkan").click(function(){
        $("#last_page_status").val('false');
        $("#page").val(1);
        ajax_request.abort();
        load_data();
    });

    var ajax_request;
    function load_data(){
        var page = $("#page").val();
        var jml_data = 10;

        var filter = $("#filter").val();
        var periode_awal = $("#periode_awal").val();
        var periode_akhir = $("#periode_akhir").val();

        var data = new Object;
        data['token'] = '<?php echo genToken('LOAD_DATA'); ?>';
        data['page'] = page;
        data['jml_data'] = jml_data;
        data['filter'] = filter;
        data['periode_awal'] = periode_awal;
        data['periode_akhir'] = periode_akhir;

        elementLoading('show', '#data_kegiatan');
        ajax_request = $.ajax({
            type: "POST",
            url: '<?php echo base_url(); ?>Kegiatan/load_data',
            data: 'data_send='+encodeURIComponent(JSON.stringify(data)),
            cache: false,
            dataType: "text",
            success: function(msg){
                elementLoading('hide', '#data_kegiatan');
                //parse JSON...
                var result = safelyParseJSON(msg);
                    $("#last_page").val(result.last_page);
                    list_data = result.result;
                    info_halaman('#info_halaman', page, result.last_page);

                var rangkai = '';
                if(list_data.length > 0){
                    for(var i=0; i < list_data.length; i++){
                        if(reformatDate(list_data[i].waktu_mulai, false, 'YYYY-MM-DD') == reformatDate(list_data[i].waktu_selesai, false, 'YYYY-MM-DD'))
                            var waktu = reformatDate(list_data[i].waktu_mulai, false, 'DD MMM YYYY HH:mm')+' - '+reformatDate(list_data[i].waktu_selesai, false, 'HH:mm');
                        else
                            var waktu = reformatDate(list_data[i].waktu_mulai, 'DD MMM YYYY HH:mm')+' - '+reformatDate(list_data[i].waktu_selesai, 'DD MMM YYYY HH:mm');

                        var btn_hapus = '<button type="button" class="btn btn-sm btn-clean btn-icon btn-icon-md" onClick="hapus('+i+', \'satuan\')" title="Hapus data"><i class="la la-trash"></i></button>';
                        if(list_data[i].id_kegiatan_parent){
                            btn_hapus = '<button type="button" class="btn btn-sm btn-clean btn-icon btn-icon-md"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="Hapus data"><i class="la la-trash"></i></button>' +
                                            '<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">' +
                                                '<a class="dropdown-item" href="#" onclick="hapus('+i+', \'satuan\')"><i class="la la-trash"></i> Kegiatan ini saja</a>' +
                                                '<a class="dropdown-item" href="#" onclick="hapus('+i+', \'satuan_dan_masadepan\')"><i class="la la-trash"></i> Kegiatan ini dan kegiatan mendatang</a>' +
                                                '<a class="dropdown-item" href="#" onclick="hapus('+i+', \'semua\')"><i class="la la-trash"></i> Semua kegiatan yang sama</a>' +
                                            '</div>';
                        }
                        rangkai += '<tr>' +
                                        '<td>'+(((page - 1) * jml_data) + i+1)+'.</td>' +
                                        '<td>'+coverMe(list_data[i].nama_kegiatan)+'<div class="kt-font-sm mb-2">'+waktu+'</div></td>' +
                                        '<td>'+coverMe(list_data[i].tempat_kegiatan)+'</td>' +
                                        '<td>'+coverMe(list_data[i].nama_kegiatan_kategori)+'</td>' +
                                        '<td>'+coverMe(list_data[i].tipe_kegiatan)+'</td>' +
                                        '<td>'+coverMe(list_data[i].tipe_berulang)+'</td>' +
                                        '<td>'+
                                            '<button type="button" class="btn btn-sm btn-clean btn-icon btn-icon-md" onClick="edit('+i+')" title="Edit data"><i class="la la-edit"></i></button>'+
                                            btn_hapus +
                                        '</td>' +
                                    '</tr>';
                    }
                }

                if(list_data.length < jml_data)
                        $("#last_page_status").val('true');


                if(rangkai){
                    $("#empty_state").remove();
                    $("#data_zona_kegiatan").show();

                    $("#data_kegiatan tbody").html(rangkai);
                }
                else{
                    if(page == 1 && filter == '')
                        create_empty_state("#data_zona_kegiatan");
                    else
                        $("#data_kegiatan tbody").html('');
                }

            }

        });
    }
    load_data();
</script>
</body>
</html>
