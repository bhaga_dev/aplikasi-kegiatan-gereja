<!DOCTYPE html>
<html lang="en" >
<!-- begin::Head -->
<head>
    <?php $this->view('include/head'); ?>
    <?php $this->view('include/css'); ?>
</head>
<!-- end::Head -->
<!-- end::Body -->

<body class="kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--fixed kt-page--loading">

<!-- begin:: Page -->
<?php $this->view('include/header_mobile');?>
<div class="kt-grid kt-grid--hor kt-grid--root">
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
        <?php $this->view('include/left_side_navbar'); ?>
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
            <?php $this->view('include/top_navbar'); ?>
            <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

                <!-- begin:: Content Head -->
                <div class="kt-subheader  kt-grid__item" id="kt_subheader">
                    <div class="kt-container  kt-container--fluid ">
                        <div class="kt-subheader__main">
                            <h3 class="kt-subheader__title">Hak Akses Menu</h3>
                        </div>
                        <div class="kt-subheader__toolbar">
                            <div class="kt-subheader__wrapper">
                            </div>
                        </div>
                    </div>
                </div>

                <!-- end:: Content Head -->

                <!-- begin:: Content -->
                <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid form_zone" id="form_administrator">
                    <div class="kt-portlet kt-portlet--mobile">
                        <div class="kt-portlet__head kt-portlet__head--noborder">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Form Hak Akses Menu
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">
                            <form id="input_form_administrator" class="kt-form kt-form--label-right" autocomplete="off">
                                <div class="form-group row">
                                    <label class="<?php echo $kolom_label; ?> col-form-label">Tipe Administrator <?php echo $red_star; ?></label>
                                    <div class="col-sm-4">
                                        <select class="form-control select2" data-placeholder="Pilih tipe administrator" id="tipe_admin" name="tipe_admin">
                                            <option selected disabled>Pilih tipe admin</option>
                                            <?php
                                            $jns_admin = $konten['jns_admin'];
                                            if($jns_admin->num_rows() > 0){
                                                foreach($jns_admin->result() as $data_jns_admin){
                                                    echo '<option value="'.$data_jns_admin->id_jns_admin.'">'.$data_jns_admin->jns_admin.'</option>';
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="<?php echo $kolom_label; ?>"></div>
                                    <div class="col-sm-9">
                                        <button class="btn btn-success waves-effect waves-light hidden" type="button" id="simpan">
                                                <span class="btn-label"><i class="la la-save"></i>
                                                </span>Simpan
                                        </button>

                                        <input type="hidden" id="index" name="index" value="0">
                                        <input type="hidden" id="action" name="action" value="save">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid tabel_zone hidden" id="menu_box">
                    <div class="kt-portlet kt-portlet--mobile">
                        <div class="kt-portlet__head kt-portlet__head--noborder">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Data Menu
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">
                            <div class="table-responsive">
                                <table class="table table-sm table-striped" id="data_menu">
                                    <thead>
                                    <tr>
                                        <th width="5%">#</th>
                                        <th>Menu</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- end:: Content -->
            </div>

            <?php $this->view('include/footer'); ?>
        </div>
    </div>
</div>

<?php $this->view('include/js'); ?>

<script>
    $("#simpan").click(function(){

        var id_jns_admin = $("#tipe_admin").val();
        var rangkai = '';
        var datajson = [];
        var jml_menu = $("#index").val();

        for(var i=0; i<jml_menu; i++){
            if($("#cb"+i).is(':checked')){
                var id_menu = $("#cb"+i).val();
                datajson.push({'id_menu': id_menu});
            }
        }

        var data = new Object;
        data['token'] = '<?php echo genToken('SEND_DATA'); ?>';
        data['id_jns_admin'] = id_jns_admin;
        data['datajson'] = datajson;

        //show loading animation...
        preloader('show');
        $.ajax({
            type: "POST",
            url: '<?php echo base_url(); ?>hak_akses_menu/simpan',
            cache: false,
            data: 'data_send='+encodeURIComponent(JSON.stringify(data)),
            dataType: "text",
            success: function(msg){//hide loading animation...
                preloader('hide');
                //parse JSON...
                var data = safelyParseJSON(msg);
                if(data.sts == 1){
                    //hapus seluruh field...
                    $("#tipe_admin").val('');

                    //reset tabel
                    $("#data_menu tbody").html('');

                    <?php echo alert('simpan_berhasil'); ?>
                    $("#simpan").hide();
                    $("#menu_box").hide();
                }
                else{
                    <?php echo alert('proses_gagal'); ?>
                }
            }
        });
    });

    $("#tipe_admin").change(function(){
        var id_jns_admin = $(this).val();
        if(id_jns_admin != '')
            load_data(id_jns_admin);
    });
    function load_data(id_jns_admin){
        $("#menu_box").show();
        $("#simpan").hide();
        var data = new Object;
        data['token'] = '<?php echo genToken('LOAD_DATA'); ?>';
        data['id_jns_admin'] = id_jns_admin;

        elementLoading('show', '#data_menu');
        $.ajax({
            type: "POST",
            url: '<?php echo base_url(); ?>hak_akses_menu/load_akses_menu',
            data: 'data_send='+encodeURIComponent(JSON.stringify(data)),
            cache: false,
            dataType: "text",
            success: function(msg){
                elementLoading('hide', '#data_menu');
                //parse JSON...
                var data = safelyParseJSON(msg);
                var rangkai = '';
                var index = 0;
                if(data.length > 0){
                    for(var i=0; i < data.length; i++){
                        rangkai += '<tr>' +
                            '<td>' +
                            '<label class="kt-checkbox kt-checkbox--bold kt-checkbox--success">' +
                            '<input id="cb'+index+'" type="checkbox" value="'+data[i].id_menu+'" '+data[i].checked+'>'+
                            '&nbsp;' +
                            '<span></span>' +
                            '</label>' +
                            '</td>' +
                            '<td>'+data[i].nama_menu+'</td>' +
                            '</tr>';

                        index = parseInt($("#index").val()) + 1;
                        $("#index").val(index);

                        //check child..
                        var submenu = safelyParseJSON(JSON.stringify(data[i].sub_menu));
                        if(submenu.length > 0){
                            for(var j=0; j< submenu.length; j++){
                                var readonly = '';
                                if(id_jns_admin == 1 && submenu[j].id_menu == '50.2'){
                                    submenu[j].checked = 'checked';
                                    readonly = 'disabled';
                                }
                                rangkai += '<tr>' +
                                    '<td>' +
                                    '<label class="kt-checkbox kt-checkbox--bold kt-checkbox--success">' +
                                    '<input type="checkbox" id="cb'+index+'" value="'+submenu[j].id_menu+'" '+submenu[j].checked+' '+readonly+'>' +
                                    '&nbsp;' +
                                    '<span></span>' +
                                    '</label>' +
                                    '</td>' +
                                    '<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+submenu[j].nama_menu+'</td>' +
                                    '</tr>';
                                index = parseInt($("#index").val()) + 1;
                                $("#index").val(index);
                            }
                        }
                    }
                    $("#index").val(index);
                }

                $("#simpan").fadeIn(200);

                $("#data_menu tbody").html(rangkai);


            }

        });
    }

</script>

</body>
<!-- end::Body -->
</html>


<script>


</script>
</body>
</html>
