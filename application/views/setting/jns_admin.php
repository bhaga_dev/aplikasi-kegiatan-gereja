<!DOCTYPE html>
<html lang="en" >
<!-- begin::Head -->
<head>
    <?php $this->view('include/head'); ?>
    <?php $this->view('include/css'); ?>
</head>
<!-- end::Head -->
<!-- end::Body -->

<body class="kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--fixed kt-page--loading">

<!-- begin:: Page -->
<?php $this->view('include/header_mobile');?>

<div class="kt-grid kt-grid--hor kt-grid--root">
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
        <?php $this->view('include/left_side_navbar'); ?>
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
            <?php $this->view('include/top_navbar'); ?>
            <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

                <!-- begin:: Content Head -->
                <div class="kt-subheader  kt-grid__item" id="kt_subheader">
                    <div class="kt-container  kt-container--fluid ">
                        <div class="kt-subheader__main">
                            <h3 class="kt-subheader__title">Tipe Administrator</h3>
                        </div>
                        <div class="kt-subheader__toolbar">
                            <div class="kt-subheader__wrapper">
                                <button class="btn btn-primary m-btn m-btn--custom m-btn--icon tabel_zone" onclick="form_action('show')">
                                    <span>
                                        <i class="la la-plus"></i>
                                        <span>
                                            Tambah Tipe Administrator
                                        </span>
                                    </span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- end:: Content Head -->

                <!-- begin:: Content -->
                <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid form_zone hidden" id="form_jns_admin">
                    <div class="kt-portlet kt-portlet--mobile">
                        <div class="kt-portlet__head kt-portlet__head--noborder">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Form Tipe Administrator
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">
                            <form id="input_form_jns_admin" class="kt-form kt-form--label-right" action="<?php echo base_url(); ?>jns_admin/simpan" method="post" autocomplete="off">
                                <input type="hidden" class="form-control" id="id_jns_admin" name="id_jns_admin" maxlength="11" placeholder="">
                                <div class="form-group row">
                                    <label class="<?php echo $kolom_label; ?> col-form-label">Tipe Administrator <?php echo $red_star; ?></label>
                                    <div class="col-sm-4">
                                        <input type="text" autocomplete="off" class="form-control" id="jns_admin" name="jns_admin" maxlength="50" placeholder="" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="<?php echo $kolom_label; ?>"></div>
                                    <div class="col-sm-9">
                                        <button class="btn btn-success waves-effect waves-light" type="submit" id="simpan">
                                                <span class="btn-label"><i class="la la-save"></i>
                                                </span>Simpan
                                        </button>
                                        <button class="btn btn-secondary waves-effect waves-light" type="button" onclick="form_action('hide')">
                                                <span class="btn-label"><i class="la la-times"></i>
                                                </span>Batal
                                        </button>
                                        <input type="hidden" id="action" name="action" value="save">
                                        <input type="hidden" id="token" name="token" value="<?php echo genToken('SEND_DATA'); ?>">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid tabel_zone" id="tabel_jns_admin">
                    <div class="kt-portlet kt-portlet--mobile">
                        <div class="kt-portlet__head kt-portlet__head--noborder">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Data administrator
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">
                            <div id="data_zona_jns_admin">
                                <div class="kt-input-icon kt-input-icon--left mb-3">
                                    <input type="text" autocomplete="off" class="form-control form-control-filter" id="filter" name="filter" placeholder="Masukkan kata kunci pencarian">
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left">
                                        <span><i class="la la-search"></i></span>
                                    </span>
                                </div>

                                <div class="table-responsive">
                                    <table class="table table-sm table-striped" id="data_jns_admin">
                                        <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Tipe Administrator</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="kt-datatable kt-datatable--default">
                                    <div class="kt-datatable__pager kt-datatable--paging-loaded">
                                        <input type="hidden" id="page" name="page" value="1">
                                        <input type="hidden" id="last_page" name="last_page">
                                        <input type="hidden" id="last_page_status" name="last_page_status" value="false">
                                        <ul class="kt-datatable__pager-nav">
                                            <li>
                                                <a title="First" class="kt-datatable__pager-link kt-datatable__pager-link--first" onclick="page('first')">
                                                    <i class="flaticon2-fast-back"></i>
                                                </a>
                                            </li>
                                            <li id="info_halaman">
                                                <a title="Previous" class="kt-datatable__pager-link kt-datatable__pager-link--prev" onclick="page('previous')">
                                                    <i class="flaticon2-back"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a title="Next" class="kt-datatable__pager-link kt-datatable__pager-link--next" onclick="page('next')">
                                                    <i class="flaticon2-next"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a title="Last" class="kt-datatable__pager-link kt-datatable__pager-link--last" onclick="page('last')">
                                                    <i class="flaticon2-fast-next"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <!-- end:: Content -->
            </div>

            <?php $this->view('include/footer'); ?>
        </div>
    </div>
</div>

<?php $this->view('include/js'); ?>

<script>
    function form_action(action){
        //reset form...
        $("#input_form_jns_admin")[0].reset();
        if(action == 'show'){
            $(".form_zone").fadeIn(300);
            $(".tabel_zone").hide();
        }
        else{
            $(".form_zone").hide();
            $(".tabel_zone").fadeIn(300);
        }
    }

    var list_data;
    $("#input_form_jns_admin").on('submit', function(e){
        e.preventDefault();

        var id_jns_admin = $("#id_jns_admin").val();
        var jns_admin = $("#jns_admin").val();

        var action = $("#action").val();

        if(!action ){
            <?php echo alert('kosong'); ?>
        }

        else{
            preloader('show');
            jQuery(this).ajaxSubmit({
                success:  function(msg){
                    var data = safelyParseJSON(msg);
                    preloader('hide');

                    if(data.sts == 1){
                        //hapus seluruh field...
                        $("#last_page_status").val('false');
                        $("#page").val(1);

                        $("#input_form_jns_admin")[0].reset();
                        $("#action").val('save');
                        form_action('hide');
                        //load data..
                        load_data();
                        <?php echo alert('simpan_berhasil'); ?>
                    }
                    else if(data.sts == 'tidak_berhak'){
                        <?php echo alert('tidak_berhak_ubah_data'); ?>
                    }
                    else{
                        <?php echo alert('proses_gagal'); ?>
                    }
                }
            });


        }
    });

    function edit(index){
        form_action('show');
        var data = list_data[index];
        $("#id_jns_admin").val(decodeHtml(data.id_jns_admin));
        $("#jns_admin").val(decodeHtml(data.jns_admin));

        $("#action").val('update');

    }
    function hapus(index){
        var data = list_data[index];
        var pertanyaan = "Apakah anda yakin ingin menghapus data "+data.jns_admin+"?";

        konfirmasi(pertanyaan, function(){
            proses_hapus(data.id_jns_admin);
        });
    }
    function proses_hapus(id){
        //show loading animation...
        preloader('show');

        var data = new Object;
        data['token'] = '<?php echo genToken('SEND_DATA'); ?>';
        data['id_jns_admin'] = id;

        $.ajax({
            type: "POST",
            url: '<?php echo base_url(); ?>Jns_admin/hapus',
            data: 'data_send='+encodeURIComponent(JSON.stringify(data)),
            cache: false,
            dataType: "text",
            success: function(msg){
                //hide loading animation...
                preloader('hide');
                //parse JSON...
                var data = safelyParseJSON(msg);

                if(data.sts == 1){
                    //load data..
                    load_data();
                    <?php echo alert('hapus_berhasil'); ?>
                }
                else if(data.sts == 'tidak_berhak'){
                    <?php echo alert('tidak_berhak_hapus_data'); ?>
                }
                else{
                    <?php echo alert('proses_gagal'); ?>
                }
            }
        });
    }

    function page(tipe){
        if(tipe == 'first'){
            $("#last_page_status").val('false');
            $("#page").val(1);
            load_data();
        }
        else if(tipe == 'previous'){
            var page = parseInt($("#page").val());
            var previous_page = page - 1;
            previous_page = (previous_page <= 1 ? 1 : previous_page);
            if(page != previous_page){
                $("#last_page_status").val('false');
                $("#page").val(previous_page);
                load_data();
            }
        }
        else if(tipe == 'next'){
            var last_page_status = $("#last_page_status").val();
            var page = parseInt($("#page").val());
            var next_page = page + 1;
            if(last_page_status == "false"){
                $("#page").val(next_page);
                load_data();
            }
        }
        else if(tipe == 'last'){
            var last_page = parseInt($("#last_page").val());
            var page = parseInt($("#page").val());
            if(last_page != page){
                $("#page").val(last_page);
                load_data();
            }
        }
        else{
            $("#page").val(tipe);
            load_data();

        }
    }

    $("#filter").keyup(function(){
        $("#last_page_status").val('false');
        $("#page").val(1);
        ajax_request.abort();
        load_data();
    });

    var ajax_request;
    function load_data(){
        var page = $("#page").val();
        var jml_data = 10;

        var filter = $("#filter").val();

        var data = new Object;
        data['token'] = '<?php echo genToken('LOAD_DATA'); ?>';
        data['page'] = page;
        data['jml_data'] = jml_data;
        data['filter'] = filter;


        elementLoading('show', '#data_jns_admin');
        ajax_request = $.ajax({
            type: "POST",
            url: '<?php echo base_url(); ?>Jns_admin/load_data',
            data: 'data_send='+encodeURIComponent(JSON.stringify(data)),
            cache: false,
            dataType: "text",
            success: function(msg){
                elementLoading('hide', '#data_jns_admin');
                //parse JSON...
                var result = safelyParseJSON(msg);
                $("#last_page").val(result.last_page);
                list_data = result.result;

                info_halaman('#info_halaman', page, result.last_page);

                var rangkai = '';
                if(list_data.length > 0){
                    for(var i=0; i < list_data.length; i++){
                        var edit = '<button type="button" class="btn btn-sm btn-clean btn-icon btn-icon-md" onClick="edit('+i+')" title="Edit data"><i class="la la-edit"></i></button>';
                        var hapus = '<button type="button" class="btn btn-sm btn-clean btn-icon btn-icon-md" onClick="hapus('+i+')" title="Hapus data"><i class="la la-trash"></i></button>';
                        if(list_data[i].id_jns_admin == 1){
                            edit = '<i>Tidak ada hak</i>';
                            hapus = '';
                        }
                        rangkai += '<tr>' +
                            '<td>'+(((page - 1) * jml_data) + i+1)+'.</td>' +
                            '<td>'+list_data[i].jns_admin+'</td>' +
                            '<td>'+

                            edit +
                            hapus +
                            '</td>' +
                            '</tr>';
                    }
                }

                if(list_data.length < jml_data)
                    $("#last_page_status").val('true');


                if(rangkai){
                    $("#empty_state").remove();
                    $("#data_zona_jns_admin").show();

                    $("#data_jns_admin tbody").html(rangkai);
                }
                else{
                    if(page == 1 && filter == '')
                        create_empty_state("#data_zona_jns_admin");
                    else
                        $("#data_jns_admin tbody").html('');
                }

            }

        });
    }
    load_data();
</script>
</body>
</html>
