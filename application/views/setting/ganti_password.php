<!DOCTYPE html>
<html lang="en" >
<!-- begin::Head -->
<head>
    <?php $this->view('include/head'); ?>
    <?php $this->view('include/css'); ?>
</head>
<!-- end::Head -->
<!-- end::Body -->

<body class="kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--fixed kt-page--loading">

<!-- begin:: Page -->
<?php $this->view('include/header_mobile');?>
<div class="kt-grid kt-grid--hor kt-grid--root">
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
        <?php $this->view('include/left_side_navbar'); ?>
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
            <?php $this->view('include/top_navbar'); ?>
            <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

                <!-- begin:: Content Head -->
                <div class="kt-subheader  kt-grid__item" id="kt_subheader">
                    <div class="kt-container  kt-container--fluid ">
                        <div class="kt-subheader__main">
                            <h3 class="kt-subheader__title">Ganti Password</h3>
                        </div>
                        <div class="kt-subheader__toolbar">
                            <div class="kt-subheader__wrapper">
                            </div>
                        </div>
                    </div>
                </div>

                <!-- end:: Content Head -->

                <!-- begin:: Content -->
                <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                    <div class="kt-portlet kt-portlet--mobile">
                        <div class="kt-portlet__head kt-portlet__head--noborder">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Form Ganti Password
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">
                            <form class="kt-form kt-form--label-right" autocomplete="off">

                                <div class="form-group row">
                                    <label class="<?php echo $kolom_label; ?> col-form-label">Password lama <?php echo $red_star; ?></label>
                                    <div class="col-sm-4">
                                        <input type="password" class="form-control" id="pwd_lama" name="pwd_lama" placeholder="Masukan password lama">

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="<?php echo $kolom_label; ?> col-form-label">Password baru <?php echo $red_star; ?></label>
                                    <div class="col-sm-4">
                                        <input type="password" class="form-control" id="pwd_baru" name="pwd_baru" placeholder="Masukan password baru">

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="<?php echo $kolom_label; ?> col-form-label">Ulangi password baru <?php echo $red_star; ?></label>
                                    <div class="col-sm-4">
                                        <input type="password" class="form-control" id="ulang_pwd_baru" name="ulang_pwd_baru" placeholder="Ulangi password baru">

                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="<?php echo $kolom_label; ?>"></div>
                                    <div class="col-sm-offset-3 col-sm-4">
                                        <button class="btn btn-success waves-effect waves-light" type="button" id="simpan">
                                                <span class="btn-label"><i class="fa fa-save"></i>
                                                </span>Simpan
                                        </button>
                                        <input type="hidden" id="action" name="action" value="save">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <!-- end:: Content -->
            </div>

            <?php $this->view('include/footer'); ?>
        </div>
    </div>
</div>

<?php $this->view('include/js'); ?>

<script>
    $("#simpan").click(function(){
        var pass_lama = $("#pwd_lama").val();
        var pass_baru = $("#pwd_baru").val();
        var ulang_pass_baru = $("#ulang_pwd_baru").val();

        if(pass_lama == '' || pass_baru == '' || ulang_pass_baru == ''){
            <?php echo alert('kosong'); ?>
        }
        else if(pass_baru != ulang_pass_baru){
            <?php echo alert('password_tidak_sama'); ?>
        }
        else{
            //show loading animation...
            preloader('show');
            //tampung value menjadi 1 varibel...
            var data = new Object;
            data['token'] = '<?php echo genToken('SEND_DATA'); ?>';
            data['pass_lama'] = pass_lama;
            data['pass_baru'] = pass_baru;

            $.ajax({
                type: "POST",
                url: '<?php echo base_url(); ?>administrator/ganti_password',
                data: 'data_send='+encodeURIComponent(JSON.stringify(data)),
                cache: false,
                dataType: "text",
                success: function(msg){
                    //hide loading animation...
                    preloader('hide');
                    //parse JSON...
                    var data = safelyParseJSON(msg);

                    if(data.sts == 1){
                        //hapus seluruh field...
                        $("#pwd_lama").val('');
                        $("#pwd_baru").val('');
                        $("#ulang_pwd_baru").val('');
                        <?php echo alert('ganti_pwd_berhasil'); ?>
                    }
                    else if(data.sts == 'password_salah'){
                        <?php echo alert('password_salah'); ?>
                    }
                    else{
                        <?php echo alert('proses_gagal'); ?>
                    }
                }

            });
        }

    });

</script>
</body>
<!-- end::Body -->
</html>
