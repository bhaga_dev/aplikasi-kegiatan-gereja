<!DOCTYPE html>
<html lang="en" >
<!-- begin::Head -->
<head>
    <?php $this->view('include/head'); ?>
    <?php $this->view('include/css'); ?>
</head>
<!-- end::Head -->
<!-- end::Body -->

<body class="kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--fixed kt-page--loading">

<!-- begin:: Page -->
<?php $this->view('include/header_mobile');?>

<div class="kt-grid kt-grid--hor kt-grid--root">
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
        <?php $this->view('include/left_side_navbar'); ?>
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
            <?php $this->view('include/top_navbar'); ?>
            <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

                <!-- begin:: Content Head -->
                <div class="kt-subheader  kt-grid__item" id="kt_subheader">
                    <div class="kt-container  kt-container--fluid ">
                        <div class="kt-subheader__main">
                            <h3 class="kt-subheader__title">
                                Keaktifan Jemaat
                            </h3>
                        </div>

                    </div>
                </div>

                <!-- end:: Content Head -->

                <!-- begin:: Content -->
                <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid" id="form_filter_box">
                    <div class="kt-portlet kt-portlet--mobile">
                        <div class="kt-portlet__head kt-portlet__head--noborder">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Filter Keaktifan Jemaat
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">
                            <form id="input_form_laporan" class="kt-form kt-form--label-right" action="<?php echo base_url(); ?>laporan/keaktifan_jemaat" method="post" autocomplete="off">
                                <div class="form-group row">
                                    <label class="<?php echo $kolom_label; ?> col-form-label">Kategori Kegiatan </label>
                                    <div class="col-sm-9">
                                        <select class="form-control select2" id="id_kegiatan_kategori" name="id_kegiatan_kategori[]"  multiple="multiple">
                                            <?php
                                            if($konten['kegiatan_kategori']->num_rows() > 0){
                                                foreach($konten['kegiatan_kategori']->result() as $list){
                                                    echo '<option value="'.$list->id_kegiatan_kategori.'">'.$list->nama_kegiatan_kategori.'</option>';
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="<?php echo $kolom_label; ?> col-form-label"><?php echo ucwords(lang('pelkat')); ?></label>
                                    <div class="col-sm-9">
                                        <select class="form-control select2" id="id_pelkat" name="id_pelkat[]"  multiple="multiple">
                                            <?php
                                            if($konten['pelkat']){
                                                for($i = 0; $i < count($konten['pelkat']); $i++){
                                                    echo '<option value="'.$konten['pelkat'][$i]->id_pelkat.'">'.$konten['pelkat'][$i]->nama_pelkat.'</option>';
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="<?php echo $kolom_label; ?> col-form-label">Periode Awal <?php echo $red_star; ?></label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control datepicker" id="periode_awal" name="periode_awal" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="<?php echo $kolom_label; ?> col-form-label">Periode Akhir <?php echo $red_star; ?></label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control datepicker" id="periode_akhir" name="periode_akhir" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="<?php echo $kolom_label; ?>"></div>
                                    <div class="col-sm-9">
                                        <button class="btn btn-success waves-effect waves-light" type="submit" id="simpan">
                                            <span class="btn-label"><i class="la la-eye"></i>
                                            </span>Tampilkan
                                        </button>

                                        <input type="hidden" id="token" name="token" value="<?php echo genToken('LOAD_DATA'); ?>">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid hidden" id="loading_bar_box">
                    <div class="kt-portlet kt-portlet--mobile">
                        <div class="kt-portlet__body" style="text-align: center">
                            <div style="width: 70%; margin: 0 auto;">
                                <h3>Proses Perhitungan Keaktifan Jemaat Sedang Berlangsung</h3>
                                <div>Mohon jangan tutup halaman ini hingga proses selesai</div>
                                <div class="kt-space-40"></div>
                                <div class="progress" style="height: 2rem;">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated  bg-success" role="progressbar" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid hidden" id="proses_selesai_box">
                    <div class="kt-portlet kt-portlet--mobile">
                        <div class="kt-portlet__body" style="text-align: center">
                            <div style="width: 70%; margin: 0 auto;">
                                <h3>Proses Perhitungan Keaktifan Jemaat Selesai</h3>
                                <div>Klik tombol dibawah ini untuk menampilkan laporan keaktifan jemaat</div>
                                <div class="kt-space-40"></div>
                                <a id="link_download" class="btn btn-primary btn-lg" style="color: #FFF" target="_blank"><i class="fa fa-download"></i> Download Laporan Keaktifan Jemaat</a>
                                <div class="kt-space-20"></div>
                                <div>
                                    <a href="<?php echo base_url(); ?>page/keaktifan_jemaat">Kembali</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end:: Content -->
            </div>

            <?php $this->view('include/footer'); ?>
        </div>
    </div>
</div>
<?php $this->view('include/js'); ?>

<script>
    $('#id_kegiatan_kategori').select2({
        placeholder: "Semua Kategori Kegiatan"
    });
    $('#id_pelkat').select2({
        placeholder: "Semua Pelkat"
    });
    $("#input_form_laporan").on('submit', function(e) {
        e.preventDefault();

        var id_pelkat = $("#id_pelkat").val();
        if(id_pelkat)
            id_pelkat = id_pelkat.join();

        preloader('show');
        $.ajax({
            type: "POST",
            url: '<?php echo base_url(); ?>laporan/keaktifan_jemaat/jml_jemaat',
            data: 'token=<?php echo genToken('LOAD_DATA'); ?>&id_pelkat='+id_pelkat,
            cache: false,
            dataType: "json",
            success: function(data){
                preloader('hide');

                if(data.sts == 1){
                    //load data..
                    var jml_jemaat = data.result;
                    if(jml_jemaat > 0){
                        var jml_data = 50;
                        var jml_page = Math.ceil(jml_jemaat / jml_data);

                        $("#form_filter_box").hide();
                        $("#loading_bar_box").show();

                        proses_hitung_jemaat(jml_jemaat, jml_data, jml_page, 1);
                    }
                    else{
                        <?php echo alert('jemaat_tdk_ada'); ?>
                    }

                }
                else{
                    <?php echo alert('proses_gagal'); ?>
                }
            }
        })
    });

    function proses_hitung_jemaat(jml_jemaat, jml_data, jml_page, page){
        var kurang = page - 1;
        if(kurang == 0)
            var persen = 1;
        else
            var persen = (kurang / jml_page) * 100;

        $(".progress-bar").css({'width': persen+'%'});

        var id_kegiatan_kategori = $("#id_kegiatan_kategori").val();
        var id_pelkat = $("#id_pelkat").val();
        var periode_awal = $("#periode_awal").val();
        var periode_akhir = $("#periode_akhir").val();
        var token = '<?php echo genToken('LOAD_DATA'); ?>';

        if(id_kegiatan_kategori)
            id_kegiatan_kategori = id_kegiatan_kategori.join();
        if(id_pelkat)
            id_pelkat = id_pelkat.join();

        $.ajax({
            type: "POST",
            url: '<?php echo base_url(); ?>laporan/keaktifan_jemaat/hitung_keaktifan_jemaat',
            data: 'token='+token+'&id_kegiatan_kategori='+id_kegiatan_kategori+'&id_pelkat='+id_pelkat+'&periode_awal='+periode_awal+'&periode_akhir='+periode_akhir+'&page='+page+'&jml_data='+jml_data,
            cache: false,
            dataType: "json",
            success: function(data){
                if(data.sts == 1){
                    //load data..
                    page++;
                    if(page <= jml_page)
                        proses_hitung_jemaat(jml_jemaat, jml_data, jml_page, page);
                    else{
                        //redirect ke halaman laporan...
                        $("#proses_selesai_box").show();
                        $("#loading_bar_box").hide();

                        var url = '<?php echo base_url(); ?>laporan/keaktifan_jemaat?token='+token+'&id_kegiatan_kategori='+id_kegiatan_kategori+'&id_pelkat='+id_pelkat+'&periode_awal='+periode_awal+'&periode_akhir='+periode_akhir;
                        $("#link_download").attr('href', url);
                    }
                }
                else{
                    <?php echo alert('proses_gagal'); ?>
                }
            }
        })

    }
</script>
</body>
</html>
