<!DOCTYPE html>
<html lang="en" >
<!-- begin::Head -->
<head>
    <?php $this->view('include/head'); ?>
    <?php $this->view('include/css'); ?>
</head>
<!-- end::Head -->
<!-- end::Body -->

<body class="kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--fixed kt-page--loading">

<!-- begin:: Page -->
<?php $this->view('include/header_mobile');?>

<div class="kt-grid kt-grid--hor kt-grid--root">
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
        <?php $this->view('include/left_side_navbar'); ?>
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
            <?php $this->view('include/top_navbar'); ?>
            <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

                <!-- begin:: Content Head -->
                <div class="kt-subheader  kt-grid__item" id="kt_subheader">
                    <div class="kt-container  kt-container--fluid ">
                        <div class="kt-subheader__main">
                            <h3 class="kt-subheader__title">
                                Tren Kehadiran
                            </h3>
                        </div>

                    </div>
                </div>

                <!-- end:: Content Head -->

                <!-- begin:: Content -->
                <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                    <div class="kt-portlet kt-portlet--mobile">
                        <div class="kt-portlet__head kt-portlet__head--noborder">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Filter Tren Kehadiran
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">
                            <form id="input_form_laporan" class="kt-form kt-form--label-right" action="<?php echo base_url(); ?>laporan/tren_kehadiran" method="get" autocomplete="off" target="_blank">
                                <div class="form-group row">
                                    <label class="<?php echo $kolom_label; ?> col-form-label">Kategori Kegiatan <?php echo $red_star; ?></label>
                                    <div class="col-sm-3">
                                        <select class="form-control select2" id="id_kegiatan_kategori" name="id_kegiatan_kategori" required>
                                            <?php
                                            if($konten['kegiatan_kategori']->num_rows() > 0){
                                                foreach($konten['kegiatan_kategori']->result() as $list){
                                                    echo '<option value="'.$list->id_kegiatan_kategori.'">'.$list->nama_kegiatan_kategori.'</option>';
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="<?php echo $kolom_label; ?> col-form-label">Periode Awal <?php echo $red_star; ?></label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control datepicker" id="periode_awal" name="periode_awal" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="<?php echo $kolom_label; ?> col-form-label">Periode Akhir <?php echo $red_star; ?></label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control datepicker" id="periode_akhir" name="periode_akhir" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="<?php echo $kolom_label; ?>"></div>
                                    <div class="col-sm-9">
                                        <button class="btn btn-success waves-effect waves-light" type="submit" id="simpan">
                                            <span class="btn-label"><i class="la la-eye"></i>
                                            </span>Tampilkan
                                        </button>

                                        <input type="hidden" id="token" name="token" value="<?php echo genToken('LOAD_DATA'); ?>">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <!-- end:: Content -->
            </div>

            <?php $this->view('include/footer'); ?>
        </div>
    </div>
</div>

<?php $this->view('include/js'); ?>

<script>

</script>
</body>
</html>
