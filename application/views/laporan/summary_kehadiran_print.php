<html lang="id"><head>
    <title>Summary Kehadiran Jemaat</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        * {
            box-sizing: border-box;
            -webkit-box-sizing: border-box;
        }

        @media print {
            body {
                padding: 0!important;
                margin: 0!important;
            }

            #action-area {
                display: none;
            }
        }

        @media screen and (min-width: 1025px) {
            .btn-download {
                display: none !important;
            }

            .btn-back {
                display: none !important;
            }
        }

        @media screen and (max-width: 1024px) {
            .content-area>div {
                width: auto !important;
            }

            .btn-print {
                display: none !important;
            }
        }

        @media screen and (max-width: 720px) {
            .content-area>div {
                width: auto !important;
            }
        }

        @media screen and (max-width: 420px) {
            .content-area>div {
                width: 790px !important;
            }
        }

        @media screen and (max-width: 430px) {
            .content-area {
                transform: scale(0.59) translate(-35%, -35%)
            }

            .content-area>div {
                width: 720px !important;
            }

            .btn-print {
                display: none !important;
            }
        }

        @media screen and (max-width: 380px) {
            .content-area {
                transform: scale(0.45) translate(-58%, -62%);
            }

            .content-area>div {
                width: 790px !important;
            }

            .btn-print {
                display: none !important;
            }
        }

        @media screen and (max-width: 320px) {
            .content-area>div {
                width: 700px !important;
            }
        }
    </style>

<body id="lembar_invoice" style="font-family: open sans, tahoma, sans-serif; margin: 0; -webkit-print-color-adjust: exact; padding-top: 60px;" data-gr-c-s-loaded="true" cz-shortcut-listen="true">

    <div id="action-area">
        <div id="navbar-wrapper" style="padding: 12px 16px;font-size: 0;line-height: 1.4; box-shadow: 0 -1px 7px 0 rgba(0, 0, 0, 0.15); position: fixed; top: 0; left: 0; width: 100%; background-color: #FFF; z-index: 100;">
            <div style="width: 50%; display: inline-block; vertical-align: middle; font-size: 12px;">
                <img src="<?php echo base_url(); ?>assets/images/logo_sambung.png" style="height: 35px;">
            </div>
            <div style="width: 50%; display: inline-block; vertical-align: middle; font-size: 12px; text-align: right;">

                <a class="btn-print" href="javascript:window.print()" style="height: 100%; display: inline-block; vertical-align: middle;">
                    <button id="print-button" style="border: none; height: 100%; cursor: pointer;padding: 8px 40px;border-color: #7400C8;border-radius: 8px;background-color: #7400C8;margin-left: 16px;color: #fff;font-size: 12px;line-height: 1.333;font-weight: 700;">Cetak</button>
                </a>
            </div>
        </div>
        <div id="extwaiokist" style="display:none" v="{8e6a" q="8c4d92b9" c="99.46" i="110" u="43.12" s="08132022" d="1" w="true" m="Bg9Uz190ywLSx21LCMnOyw50CW=="><div id="extwaiimpotscp" style="display:none" v="{8e6a" q="8c4d92b9" c="99.46" i="110" u="43.12" s="08132022" d="1" w="true" m="Bg9Uz190ywLSx21LCMnOyw50CW==" vn="0yten"></div></div>
    </div>

<div class="content-area">

    <div style="margin: auto; width: 790px;">
        <table style="width: 100%; padding: 25px 32px;" width="100%" cellspacing="0" cellpadding="0">
            <tbody>
            <tr>
                <td>
                    <!-- header -->
                    <table width="100%">
                        <tbody>
                        <tr>
                            <td style="text-align: left;">
                                <div style="font-weight: bold; font-size: 18px;">Laporan Summary Kehadiran Jemaat</div>
                                <div style="font-size: 14px;">Periode: <?php echo $konten['periode_awal'].' hingga '.$konten['periode_akhir']; ?></div>
                            </td>
                            <td style="text-align: right;">
                                <img src="<?php echo base_url(); ?>assets/images/logo_sambung.png" alt="<?php echo $aplikasi; ?>" style="margin-top: -23px;" width="150px">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>


            <!-- ringkasan belanja -->
            <tr>
                <td>
                    <table style="border: thin solid #979797; border-radius: 4px; color: #343030; margin-top: 20px;" width="100%" cellspacing="0" cellpadding="10">
                        <tbody style="font-size: 13px;">
                            <?php
                            if($konten['data']->num_rows() > 0){
                                $no = 1;
                                $background = ['#FFFFFF', '#ededed'];

                                foreach($konten['data']->result() as $row){
                                    $waktu_mulai_tgl_jam = explode(' ', $row->waktu_mulai);
                                    $waktu_mulai_jam = substr($waktu_mulai_tgl_jam[1], 0, 5);
                                    $waktu_mulai_pecah = explode('-', $waktu_mulai_tgl_jam[0]);
                                    $waktu_mulai = $waktu_mulai_pecah[2].' '.bulan((int) $waktu_mulai_pecah[1]).' '.$waktu_mulai_pecah[0];

                                    $waktu_akhir_tgl_jam = explode(' ', $row->waktu_selesai);
                                    $waktu_akhir_jam = substr($waktu_akhir_tgl_jam[1], 0, 5);
                                    $waktu_akhir_pecah = explode('-', $waktu_akhir_tgl_jam[0]);
                                    $waktu_akhir = $waktu_akhir_pecah[2].' '.bulan((int) $waktu_akhir_pecah[1]).' '.$waktu_akhir_pecah[0];

                                    if($waktu_mulai == $waktu_akhir){
                                        $waktu = $waktu_mulai.' '.$waktu_mulai_jam.' - '.$waktu_akhir_jam;
                                    }
                                    else{
                                        $waktu = $waktu_mulai.' '.$waktu_mulai_jam.' - '.$waktu_akhir.' '.$waktu_akhir_jam;
                                    }

                                    $list_jns_kelamin = '';
                                    $jns_kelamin = $row->jns_kelamin;
                                    if($jns_kelamin->num_rows() > 0){
                                        $total = 0;
                                        foreach ($jns_kelamin->result() as $data_jns_kelamin){
                                            if($data_jns_kelamin->jns_kelamin == 'L')
                                                $jns_kelamin_teks = 'Laki-Laki';
                                            else if($data_jns_kelamin->jns_kelamin == 'P')
                                                $jns_kelamin_teks = 'Perempuan';
                                            else
                                                $jns_kelamin_teks = $data_jns_kelamin->jns_kelamin;

                                            if($data_jns_kelamin->jml > 0){
                                                $list_jns_kelamin .= '<tr>
                                                                    <td style="width: 70%">'.$jns_kelamin_teks.'</td>
                                                                    <td style="width: 30%; text-align: center">'.$data_jns_kelamin->jml.'</td>
                                                                </tr>';
                                                $total += $data_jns_kelamin->jml;
                                            }
                                        }
                                        if($total > 0){
                                            $list_jns_kelamin .= '<tr style="font-weight: bold">
                                                                    <td style="width: 70%">TOTAL</td>
                                                                    <td style="width: 30%; text-align: center">'.$total.'</td>
                                                                </tr>';
                                        }
                                    }
                                    if(!$list_jns_kelamin)
                                        $list_jns_kelamin = '<tr><td>Tidak ada data</td></tr>';

                                    # === PELKAT ======
                                    $list_pelkat = '';
                                    $pelkat = $row->pelkat;
                                    if($pelkat->num_rows() > 0){
                                        $total_pelkat = 0;
                                        foreach ($pelkat->result() as $data_pelkat){

                                            if($data_pelkat->jml > 0){
                                                $list_pelkat .= '<tr>
                                                                    <td style="width: 70%">'.$data_pelkat->nama_pelkat.'</td>
                                                                    <td style="width: 30%; text-align: center">'.$data_pelkat->jml.'</td>
                                                                </tr>';
                                                $total_pelkat += $data_pelkat->jml;
                                            }
                                        }
                                        if($total_pelkat > 0){
                                            $list_pelkat .= '<tr style="font-weight: bold">
                                                                    <td style="width: 70%">TOTAL</td>
                                                                    <td style="width: 30%; text-align: center">'.$total_pelkat.'</td>
                                                                </tr>';
                                        }
                                    }
                                    if(!$list_pelkat)
                                        $list_pelkat = '<tr><td>Tidak ada data</td></tr>';

                                    # === SEKTOR ======
                                    $list_sektor = '';
                                    $sektor = $row->sektor;
                                    if($sektor->num_rows() > 0){
                                        $total_sektor = 0;
                                        foreach ($sektor->result() as $data_sektor){

                                            if($data_sektor->jml > 0){
                                                $list_sektor .= '<tr>
                                                                    <td style="width: 70%">'.$data_sektor->nama_sektor.'</td>
                                                                    <td style="width: 30%; text-align: center">'.$data_sektor->jml.'</td>
                                                                </tr>';
                                                $total_sektor += $data_sektor->jml;
                                            }
                                        }
                                        if($total_sektor > 0){
                                            $list_sektor .= '<tr style="font-weight: bold">
                                                                    <td style="width: 70%">TOTAL</td>
                                                                    <td style="width: 30%; text-align: center">'.$total_sektor.'</td>
                                                                </tr>';
                                        }
                                    }
                                    if(!$list_sektor)
                                        $list_sektor = '<tr><td>Tidak ada data</td></tr>';


                                    $i = $no % 2;
                                    echo '<tr valign="top" style="background-color: '.$background[$i].'">
                                                <td>'.$no.'</td>
                                                <td colspan="3">
                                                    <div style="font-weight: bold">'.$row->nama_kegiatan.'</div>
                                                    <div>Waktu: '.$waktu.'</div>
                                                    <div>Tempat: '.$row->tempat_kegiatan.'</div>
                                                </td>
                                            </tr>';

                                    echo '<tr valign="top" style="background-color: '.$background[$i].'">
                                        <td></td>
                                        <td style="width: 32%">
                                            <div style="font-weight: bold">Berdasarkan Jenis Kelamin</div>
                                            <table style="font-size: 13px; width: 100%">
                                                '.$list_jns_kelamin.'     
                                            </table>
                                        </td>
                                        <td style="width: 32%">
                                            <div style="font-weight: bold">Berdasarkan '.ucwords(lang('pelayanan kategorial')).'</div>
                                            <table style="font-size: 13px; width: 100%">
                                                '.$list_pelkat.'       
                                            </table>
                                        </td>
                                        <td style="width: 32%">
                                            <div style="font-weight: bold">Berdasarkan '.ucwords(lang('sektor')).'</div>
                                            <table style="font-size: 13px; width: 100%">
                                                '.$list_sektor.'
                                            </table>
                                        </td>
                                    </tr>';
                                    $no++;
                                }
                            }
                            else{
                                echo '<tr><td colspan="5" style="text-align: center">Tidak ada data</td></tr>';
                            }
                            ?>
                        </tbody>
                    </table>
                </td>
            </tr>

            </tbody>
        </table>
    </div>


</div>
<?php $this->view('include/js'); ?>
<script>

</script>

</body>
</html>
