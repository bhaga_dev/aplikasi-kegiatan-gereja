<html lang="id"><head>
    <title>Keaktifan Jemaat</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        * {
            box-sizing: border-box;
            -webkit-box-sizing: border-box;
        }

        @media print {
            body {
                padding: 0!important;
                margin: 0!important;
            }

            #action-area {
                display: none;
            }
        }

        @media screen and (min-width: 1025px) {
            .btn-download {
                display: none !important;
            }

            .btn-back {
                display: none !important;
            }
        }

        @media screen and (max-width: 1024px) {
            .content-area>div {
                width: auto !important;
            }

            .btn-print {
                display: none !important;
            }
        }

        @media screen and (max-width: 720px) {
            .content-area>div {
                width: auto !important;
            }
        }

        @media screen and (max-width: 420px) {
            .content-area>div {
                width: 790px !important;
            }
        }

        @media screen and (max-width: 430px) {
            .content-area {
                transform: scale(0.59) translate(-35%, -35%)
            }

            .content-area>div {
                width: 720px !important;
            }

            .btn-print {
                display: none !important;
            }
        }

        @media screen and (max-width: 380px) {
            .content-area {
                transform: scale(0.45) translate(-58%, -62%);
            }

            .content-area>div {
                width: 790px !important;
            }

            .btn-print {
                display: none !important;
            }
        }

        @media screen and (max-width: 320px) {
            .content-area>div {
                width: 700px !important;
            }
        }
    </style>

<body id="lembar_invoice" style="font-family: open sans, tahoma, sans-serif; margin: 0; -webkit-print-color-adjust: exact; padding-top: 60px;" data-gr-c-s-loaded="true" cz-shortcut-listen="true">

    <div id="action-area">
        <div id="navbar-wrapper" style="padding: 12px 16px;font-size: 0;line-height: 1.4; box-shadow: 0 -1px 7px 0 rgba(0, 0, 0, 0.15); position: fixed; top: 0; left: 0; width: 100%; background-color: #FFF; z-index: 100;">
            <div style="width: 50%; display: inline-block; vertical-align: middle; font-size: 12px;">
                <img src="<?php echo base_url(); ?>assets/images/logo_sambung.png" style="height: 35px;">
            </div>
            <div style="width: 50%; display: inline-block; vertical-align: middle; font-size: 12px; text-align: right;">

                <a class="btn-print" href="javascript:window.print()" style="height: 100%; display: inline-block; vertical-align: middle;">
                    <button id="print-button" style="border: none; height: 100%; cursor: pointer;padding: 8px 40px;border-color: #7400C8;border-radius: 8px;background-color: #7400C8;margin-left: 16px;color: #fff;font-size: 12px;line-height: 1.333;font-weight: 700;">Cetak</button>
                </a>
            </div>
        </div>
        <div id="extwaiokist" style="display:none" v="{8e6a" q="8c4d92b9" c="99.46" i="110" u="43.12" s="08132022" d="1" w="true" m="Bg9Uz190ywLSx21LCMnOyw50CW=="><div id="extwaiimpotscp" style="display:none" v="{8e6a" q="8c4d92b9" c="99.46" i="110" u="43.12" s="08132022" d="1" w="true" m="Bg9Uz190ywLSx21LCMnOyw50CW==" vn="0yten"></div></div>
    </div>

<div class="content-area">

    <div style="margin: auto; width: 790px;">
        <table style="width: 100%; padding: 25px 32px;" width="100%" cellspacing="0" cellpadding="0">
            <tbody>
            <tr>
                <td>
                    <!-- header -->
                    <table width="100%">
                        <tbody>
                        <tr>
                            <td style="text-align: left;">
                                <div style="font-weight: bold; font-size: 18px;">Laporan Data Keaktifan Jemaat</div>
                                <div style="font-size: 14px;">Kategori: <?php echo $konten['kategori_kegiatan']; ?></div>
                                <div style="font-size: 14px;"><?php echo ucwords(lang('pelkat'))?>: <?php echo $konten['pelkat']; ?></div>
                                <div style="font-size: 14px;">Periode: <?php echo $konten['periode_awal'].' hingga '.$konten['periode_akhir']; ?></div>
                            </td>
                            <td style="text-align: right;">
                                <img src="<?php echo base_url(); ?>assets/images/logo_sambung.png" alt="<?php echo $aplikasi; ?>" style="margin-top: -23px;" width="150px">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>


            <!-- ringkasan belanja -->
            <tr>
                <td>
                    <table style="border: thin solid #979797; border-radius: 4px; color: #343030; margin-top: 20px; font-size: 13px;" width="100%" cellspacing="0" cellpadding="5">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Kode Jemaat</th>
                                <th>Nama Jemaat</th>
                                <th>Jml Kegiatan</th>
                                <th>Kehadiran</th>
                                <th>Persentase</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody style="font-size: 13px;">
                            <?php
                            $jml_tidak_aktif = 0;
                            $jml_kurang_aktif = 0;
                            $jml_cukup_aktif = 0;
                            $jml_aktif = 0;
                            $jml_sangat_aktif = 0;
                            $jml_jemaat = $konten['keaktifan_jemaat']->num_rows();

                            $old_sektor = '';
                            if($konten['keaktifan_jemaat']->num_rows() > 0){
                                $no = 1;
                                $detail_summary = array();
                                $index = -1;

                                foreach ($konten['keaktifan_jemaat']->result() as $keaktifan_jemaat){
                                    if($keaktifan_jemaat->jml_ikut_kegiatan > 0)
                                        $persentase = floor(($keaktifan_jemaat->jml_ikut_kegiatan / $keaktifan_jemaat->jml_kegiatan_terselenggara) * 100);
                                    else
                                        $persentase = 0;


                                    if($old_sektor != $keaktifan_jemaat->nama_sektor){
                                        echo '<tr>
                                                    <td colspan="7"><b>'.$keaktifan_jemaat->nama_sektor.'</b></td>
                                                </tr>';
                                        $old_sektor = $keaktifan_jemaat->nama_sektor;
                                        $index++;
                                        $detail_summary[] = array($keaktifan_jemaat->nama_sektor, array('tidak_aktif' => 0, 'kurang_aktif' => 0, 'cukup_aktif' => 0, 'aktif' => 0, 'sangat_aktif' => 0), 0);
                                    }

                                    if($persentase <= 20){
                                        $status = 'Tidak Aktif';
                                        $jml_tidak_aktif++;

                                        $detail_summary[$index][1]['tidak_aktif']++;
                                    }
                                    else if($persentase >= 21 and $persentase <= 40){
                                        $status = 'Kurang Aktif';
                                        $jml_kurang_aktif++;
                                        $detail_summary[$index][1]['kurang_aktif']++;
                                    }
                                    else if($persentase >= 41 and $persentase <= 60){
                                        $status = 'Cukup Aktif';
                                        $jml_cukup_aktif++;
                                        $detail_summary[$index][1]['cukup_aktif']++;
                                    }
                                    else if($persentase >= 61 and $persentase <= 80){
                                        $status = 'Aktif';
                                        $jml_cukup_aktif++;
                                        $detail_summary[$index][1]['aktif']++;
                                    }
                                    else if($persentase >= 81 and $persentase <= 100){
                                        $status = 'Sangat Aktif';
                                        $jml_sangat_aktif++;
                                        $detail_summary[$index][1]['sangat_aktif']++;
                                    }

                                    $detail_summary[$index][2]++;

                                    echo '<tr style="text-align: center;">
                                                <td>'.$no.'</td>
                                                <td>'.$keaktifan_jemaat->kode_jemaat.'</td>
                                                <td>'.$keaktifan_jemaat->nama_jemaat.'</td>
                                                <td>'.$keaktifan_jemaat->jml_kegiatan_terselenggara.'</td>
                                                <td>'.$keaktifan_jemaat->jml_ikut_kegiatan.'</td>
                                                <td>'.$persentase.'%</td>
                                                <td>'.$status.'</td>
                                            </tr>';

                                    $no++;
                                }
                            }
                            else{
                                echo '<tr><td colspan="5">Tidak ada data</td></tr>';
                            }

                            $persen_tidak_aktif = 0;
                            if($jml_tidak_aktif > 0)
                                $persen_tidak_aktif = floor(($jml_tidak_aktif / $jml_jemaat) * 100);

                            $persen_kurang_aktif = 0;
                            if($jml_kurang_aktif > 0)
                            $persen_kurang_aktif = floor(($jml_kurang_aktif / $jml_jemaat) * 100);

                            $persen_cukup_aktif = 0;
                            if($jml_cukup_aktif > 0)
                            $persen_cukup_aktif = floor(($jml_cukup_aktif / $jml_jemaat) * 100);

                            $persen_aktif = 0;
                            if($jml_aktif > 0)
                            $persen_aktif = floor(($jml_aktif / $jml_jemaat) * 100);

                            $persen_sangat_aktif = 0;
                            if($jml_sangat_aktif > 0)
                            $persen_sangat_aktif = floor(($jml_sangat_aktif / $jml_jemaat) * 100);
                            ?>
                        </tbody>
                    </table>
                    <h4>Summary Keaktifan Jemaat</h4>
                    <table style="font-size:13px; border: thin solid #979797; border-radius: 4px; color: #343030; margin-top: 20px;" width="100%" cellspacing="0" cellpadding="10">
                        <thead>
                        <tr>
                            <th>Persentase Keaktifan</th>
                            <th>Status</th>
                            <th>Jumlah Jemaat</th>
                            <th>Persentase Jemaat</th>
                        </tr>
                        </thead>
                        <tbody style="text-align: center">
                        <tr>
                            <td>0% - 20%</td>
                            <td>Tidak Aktif</td>
                            <td><?php echo rupiah($jml_tidak_aktif); ?> Jemaat</td>
                            <td><?php echo $persen_tidak_aktif; ?>%</td>
                        </tr>
                        <tr>
                            <td>21% - 40%</td>
                            <td>Kurang Aktif</td>
                            <td><?php echo rupiah($jml_kurang_aktif); ?> Jemaat</td>
                            <td><?php echo $persen_kurang_aktif; ?>%</td>
                        </tr>
                        <tr>
                            <td>41% - 60%</td>
                            <td>Cukup Aktif</td>
                            <td><?php echo rupiah($jml_cukup_aktif); ?> Jemaat</td>
                            <td><?php echo $persen_cukup_aktif; ?>%</td>
                        </tr>
                        <tr>
                            <td>61% - 80%</td>
                            <td>Aktif</td>
                            <td><?php echo rupiah($jml_aktif); ?> Jemaat</td>
                            <td><?php echo $persen_aktif; ?>%</td>
                        </tr>
                        <tr>
                            <td>81% - 100%</td>
                            <td>Sangat Aktif</td>
                            <td><?php echo rupiah($jml_sangat_aktif); ?> Jemaat</td>
                            <td><?php echo $persen_sangat_aktif; ?>%</td>
                        </tr>
                        </tbody>

                    </table>

                    <h4>Detail Keaktifan Jemaat</h4>
                    <?php
                    if(count($detail_summary) > 0){
                        for($i = 0; $i < count($detail_summary); $i++){
                            echo '<h5>'.$detail_summary[$i][0].'</h5>';

                            $jml_jemaat = $detail_summary[$i][2];
                            $jml_tidak_aktif = $detail_summary[$i][1]['tidak_aktif'];
                            $persen_tidak_aktif = 0;
                            if($jml_tidak_aktif > 0)
                                $persen_tidak_aktif = floor(($jml_tidak_aktif / $jml_jemaat) * 100);

                            $persen_kurang_aktif = 0;
                            $jml_kurang_aktif = $detail_summary[$i][1]['kurang_aktif'];
                            if($jml_kurang_aktif > 0)
                                $persen_kurang_aktif = floor(($jml_kurang_aktif / $jml_jemaat) * 100);

                            $persen_cukup_aktif = 0;
                            $jml_cukup_aktif = $detail_summary[$i][1]['cukup_aktif'];
                            if($jml_cukup_aktif > 0)
                                $persen_cukup_aktif = floor(($jml_cukup_aktif / $jml_jemaat) * 100);

                            $persen_aktif = 0;
                            $jml_aktif = $detail_summary[$i][1]['aktif'];
                            if($jml_aktif > 0)
                                $persen_aktif = floor(($jml_aktif / $jml_jemaat) * 100);

                            $persen_sangat_aktif = 0;
                            $jml_sangat_aktif = $detail_summary[$i][1]['sangat_aktif'];
                            if($jml_sangat_aktif > 0)
                                $persen_sangat_aktif = floor(($jml_sangat_aktif / $jml_jemaat) * 100);

                            ?>
                            <table style="font-size:13px; border: thin solid #979797; border-radius: 4px; color: #343030; margin-top: 20px;" width="100%" cellspacing="0" cellpadding="10">
                                <thead>
                                <tr>
                                    <th>Persentase Keaktifan</th>
                                    <th>Status</th>
                                    <th>Jumlah Jemaat</th>
                                    <th>Persentase Jemaat</th>
                                </tr>
                                </thead>
                                <tbody style="text-align: center">
                                <tr>
                                    <td>0% - 20%</td>
                                    <td>Tidak Aktif</td>
                                    <td><?php echo rupiah($jml_tidak_aktif); ?> Jemaat</td>
                                    <td><?php echo $persen_tidak_aktif; ?>%</td>
                                </tr>
                                <tr>
                                    <td>21% - 40%</td>
                                    <td>Kurang Aktif</td>
                                    <td><?php echo rupiah($jml_kurang_aktif); ?> Jemaat</td>
                                    <td><?php echo $persen_kurang_aktif; ?>%</td>
                                </tr>
                                <tr>
                                    <td>41% - 60%</td>
                                    <td>Cukup Aktif</td>
                                    <td><?php echo rupiah($jml_cukup_aktif); ?> Jemaat</td>
                                    <td><?php echo $persen_cukup_aktif; ?>%</td>
                                </tr>
                                <tr>
                                    <td>61% - 80%</td>
                                    <td>Aktif</td>
                                    <td><?php echo rupiah($jml_aktif); ?> Jemaat</td>
                                    <td><?php echo $persen_aktif; ?>%</td>
                                </tr>
                                <tr>
                                    <td>81% - 100%</td>
                                    <td>Sangat Aktif</td>
                                    <td><?php echo rupiah($jml_sangat_aktif); ?> Jemaat</td>
                                    <td><?php echo $persen_sangat_aktif; ?>%</td>
                                </tr>
                                </tbody>

                            </table>
                            <?php
                        }
                    }
                    else{
                        echo 'Tidak ada data';
                    }
                    ?>
                </td>
            </tr>

            </tbody>
        </table>
    </div>


</div>
<?php $this->view('include/js'); ?>
<script>

</script>

</body>
</html>
