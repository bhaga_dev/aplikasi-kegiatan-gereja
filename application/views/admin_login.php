<!DOCTYPE html>
<html lang="en" >
	<!-- begin::Head -->
	<head>

        <?php $this->view('include/head'); ?>
		<!--begin::Web font -->
        <!--begin::Page Custom Styles(used by this page) -->
        <link href="<?php echo base_url(); ?>assets/css/pages/login/login-3.css" rel="stylesheet" type="text/css" />
        <?php $this->view('include/css'); ?>

	</head>
	<!-- end::Head -->
    <!-- end::Body -->
    <body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">
		<!-- begin:: Page -->
        <div class="kt-grid kt-grid--ver kt-grid--root">
            <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v3 kt-login--signin" id="kt_login">
                <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" style="background-repeat: no-repeat; background-color: #FFF; background-attachment: fixed; background-position: left; background-image: url('<?php echo base_url(); ?>assets/images/welcome.png'); background-size: auto 100%">
                    <div class="kt-grid__item kt-grid__item--fluid kt-login__wrapper">
                        <div class="kt-login__container" style="background: #FFF; border-radius: 7px; padding: 20px; box-shadow: 0 0 28px 10px rgba(82,63,105,0.08)">
                            <div class="kt-login__logo">
                                <a href="#">
                                    <img src="<?php echo base_url(); ?>assets/images/logo_sambung.png" style="height: 50px">
                                </a>
                            </div>
                            <div class="kt-login__signin">
                                <div class="kt-login__head">
                                    <h3 class="kt-login__title">Sign In To Admin</h3>
                                </div>
                                <form class="kt-form" action="">
                                    <div class="input-group">
                                        <input class="form-control m-input"   type="text" placeholder="Username" name="username" autocomplete="off" id="username" required>
                                    </div>
                                    <div class="input-group">
                                        <input class="form-control m-input m-login__form-input--last" type="password" placeholder="Password" name="password" id="password" required>
                                    </div>
                                    <div class="kt-login__actions">
                                        <button type="button" id="login"  class="btn btn-brand btn-elevate kt-login__btn-primary">Sign In</button>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php $this->view('include/js'); ?>

        <script>

            $('#username').keyup(function(e) {
                if(e.which == 13) {
                    e.preventDefault();
                    login();
                }
            });
            $('#password').keyup(function(e) {
                if(e.which == 13) {
                    e.preventDefault();
                    login();
                }
            });

            $("#login").click(function(){
                login();
            });

            function login(){
                var username = $("#username").val();
                var password = $("#password").val();

                if(username == '' || password == ''){
                    <?php echo alert('kosong'); ?>
                }
                else{
                    //show loading animation...
                    $("#login").addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
                    //tampung value menjadi 1 varibel...
                    var data = new Object;
                    data['token'] = '<?php echo genToken('SEND_DATA'); ?>';
                    data['username'] = username;
                    data['password'] = password;

                    $.ajax({
                        type: "POST",
                        url: '<?php echo base_url(); ?>gateway/login',
                        data: 'data_send='+encodeURIComponent(JSON.stringify(data)),
                        cache: false,
                        dataType: "text",
                        success: function(msg){
                            console.log(msg);
                            //hide loading animation...
                            $("#login").removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').removeAttr('disabled');


                            //parse JSON...
                            var data = safelyParseJSON(msg);

                            if(data.sts == 1){
                                <?php echo alert('login_berhasil'); ?>
                                location.href = data.redirect;
                            }
                            else if(data.sts == 'not_valid'){
                                <?php echo alert('not_valid'); ?>
                            }
                            else{
                                <?php echo alert('proses_gagal'); ?>
                            }
                        }

                    });
                }
            }
        </script>
	</body>
	<!-- end::Body -->
</html>
