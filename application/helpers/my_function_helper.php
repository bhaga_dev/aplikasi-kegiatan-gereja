<?php defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('maxlength'))
{
    function maxlength($string, $num)
    {
        if(strlen($string) > $num)
            $string = substr($string, 0, $num).'...';
        return $string;
    }
}
if ( ! function_exists('reformat_date'))
{
    function reformat_date($date)
    {
        $time = explode(' ', $date);
        $jam = '';
        if(array_key_exists("1",$time)){
            $pecah = explode(":", $time[1]);
            $jam = ' '.$pecah[0].':'.$pecah[1];
        }

        $tanggal = explode('-', $time[0]);
        $tanggal = $tanggal[0].' '.bulan((int) $tanggal[1]).' '.$tanggal[2].$jam;

        return $tanggal;
    }
}
if ( ! function_exists('genToken'))
{
    function genToken($type_token)
    {
        $CI = &get_instance();
        return $CI->getToken($type_token);
    }
}
if ( ! function_exists('alert'))
{
    function alert($i)
    {
        $CI = &get_instance();
        return $CI->alert($i);
    }
}
if ( ! function_exists('coverMe'))
{
    function coverMe($teks, $mask = '')
    {
        if($teks)
            return $teks;
        else{
            if($mask)
                return $mask;
            else
                return '-';
        }
    }
}
if ( ! function_exists('bulan'))
{
    function bulan($i)
    {
        $CI = &get_instance();
        return $CI->bulan($i);
    }
}
if ( ! function_exists('balik_tanggal'))
{
    function balik_tanggal($date)
    {
        $time = explode(' ', $date);
        $jam = '';
        if(array_key_exists("1",$time)){
            $pecah = explode(":", $time[1]);
            $jam = ' '.$pecah[0].':'.$pecah[1];
        }


        $tanggal = explode('-', $time[0]);
        $tanggal = $tanggal[2].'-'.$tanggal[1].'-'.$tanggal[0].$jam;

        return $tanggal;
    }
}

if ( ! function_exists('_lang'))
{
    function _lang($pesan)
    {
        $CI = &get_instance();
        $hasil = $CI->lang($pesan);
        echo $hasil;
    }
}
if ( ! function_exists('lang'))
{
    function lang($pesan)
    {
        $CI = &get_instance();
        $hasil = $CI->lang($pesan);
        return $hasil;
    }
}
if ( ! function_exists('all_lang'))
{
    function all_lang()
    {
        $CI = &get_instance();
        $your_language_array = $CI->lang->load('publik_lang.php','id', TRUE);
        return $your_language_array;
    }
}
if ( ! function_exists('rupiah'))
{
    function rupiah($angka)
    {
        $CI = &get_instance();
        $hasil = $CI->rupiah($angka);
        return $hasil;
    }
}
