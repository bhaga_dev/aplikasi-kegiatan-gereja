toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": true,
    "progressBar": false,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "7000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
};
window.preloader = function(action){
    if(action == 'show'){

        KTApp.blockPage({
            overlayColor: '#000000',
            type: 'v2',
            state: 'success',
            message: 'Loading...'
        });

    }
    else{
        KTApp.unblockPage();
    }
}
window.elementLoading = function(action, element){
    if(action == 'show'){
        KTApp.block(element, {
            overlayColor: '#000000',
            state: 'primary'
        });

    }
    else{
        KTApp.unblock(element);
    }

}
window.konfirmasi = function(pertanyaan, callback_yes, callback_no = null, btn_ya = 'Ya, lanjutkan!', btn_tidak = 'Tidak, batalkan!', ){
    swal.fire({
        title: 'Konfirmasi',
        text: pertanyaan,
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: btn_ya,
        cancelButtonText: btn_tidak,
        confirmButtonColor: '#0abb87',
        cancelButtonColor: '#d33',
        reverseButtons: true
    }).then(function(result){
        if (result.value) {
            callback_yes();
        } else if (result.dismiss === 'cancel') {
            callback_no;
        }
    });

}
window.create_empty_state = function(element, pesan_tambah = ''){
    var html = '<div style="text-align: center" id="empty_state"><img src="'+base_url+'assets/images/empty.png" width="200px">' +
        '<h5>Anda tidak memiliki data ini.</h5>'+pesan_tambah+'</div>';
    $(element).hide();
    $(element).after(html);
}

window.validateEmail = function(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
};
window.reformatDate = function(date, time, format){
    moment.locale('id');
    if(format){
        return moment(date).format(format);
    }
    else{
        if(time)
            return moment(date).format('DD MMMM YYYY HH:mm');
        else
            return moment(date).format('DD MMMM YYYY');
    }
}
window.isDate = function(tanggal, time = false) {
    if(time){
        var pisah = tanggal.split(' ');
        var pecah = pisah[0].split('-');
        tanggal = pecah[2]+'-'+pecah[1]+'-'+pecah[0]+' '+pisah[1];
    }
    else{
        var pecah = tanggal.split('-');
        tanggal = pecah[2]+'-'+pecah[1]+'-'+pecah[0];
    }
    return moment(tanggal).isValid();
}

$(".numeric").keydown(function(event) {
    if((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 190 || event.keyCode == 8 || event.keyCode == 9 ){
        // Allow. let it happen, don't do anything
    }
    else{
        //reject..
        event.preventDefault();
    }
});
window.render_status_label = function(v_status, v_status_text){
    var status = '';
    if(v_status == 'A'){
        if(!v_status_text)
            v_status_text = 'Aktif';
        status = render_badge('kt-badge--info', v_status_text);
    }
    else if(v_status == 'N'){
        if(!v_status_text)
            v_status_text = 'Tidak aktif';
        status = render_badge('kt-badge--danger', v_status_text);
    }
    else{
        if(!v_status_text)
            v_status_text = 'Tidak diketahui';
        status = render_badge('kt-badge--dark', v_status_text);
    }
    return status;
};
window.render_badge = function(style, text){
    var badge = '<span class="kt-badge kt-badge--inline '+style+'">'+text+'</span>';
    return badge;
};
var default_table = {"bLengthChange": false,
    "bFilter": false,
    "bInfo": false,
    "bPaginate": false,
    "bOrderable": false,
    "bSort": false
};

//mencegah submit saat tekan tombol enter...
$('form').bind("keydown", function(e) {
    if ((e.keyCode == 13)  && ($(e.target)[0]!=$("textarea")[0]) ) {
        e.preventDefault();
        return false;
    }
});
window.hideBr = function(str){
    str.replace(/<br\s*[\/]?>/gi, "\n");
};
window.decodeHtml = function(html) {
    var txt = document.createElement("textarea");
    txt.innerHTML = html;
    return txt.value;
};
window.rupiah = function(angka)
{
    var rupiah = '';
    var angkarev = angka.toString().split('').reverse().join('');
    for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
    return rupiah.split('',rupiah.length-1).reverse().join('');
};
window.convertToAngka = function(rupiah)
{
    return parseInt(rupiah.replace(/,.*|[^0-9]/g, ''), 10);
};
window.convertToRupiah = function(element){
    var angka = $(element).val();
    angka = convertToAngka(angka);
    $(element).val(rupiah(angka));
};
window.safelyParseJSON = function(json){
    try {
        return JSON.parse(json);
    } catch(ex){
        console.log(json);
        var data = new Object;
        data['sts'] = 'x';
        return JSON.stringify(data);
    }
};
window.coverMe = function(value, cover){
    if(!value){
        if(!cover)
            return '-';
        else
            return cover;
    }
    else
        return value;
};
window.generate_img = function(element, buffer, action_success, action_failed){
    const file =  $(element)[0].files[0];

    if(!file){
        return;
    }

    $(element).parent().append('<div id="temp_loading"><i class="fa fa-spinner fa-spin"></i> Loading...</div>');
    new ImageCompressor(file, {
        quality: 0.6,
        maxWidth: 1000,
        maxHeight: 1000,
        convertSize: 5000000,
        success(blob) {
            var reader = new FileReader();
            reader.readAsDataURL(blob);
            reader.onloadend = function() {
                base64data = reader.result;
                $(buffer).attr('src', base64data);
                $("#temp_loading").remove();

                if(action_success)
                    action_success(base64data);
            }
        },
        error(e) {
            if(action_failed)
                action_failed(e.message);
            $("#temp_loading").remove();
        },
    });
};
window.initDatepicker = function(){
    $('.datepicker').datepicker({
        todayHighlight: true,
        format: 'dd-mm-yyyy',
        autoclose: true,
        orientation: "bottom left",
        templates: {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    });
};
initDatepicker();

$('.datetimepicker').datetimepicker({
    todayHighlight: true,
    autoclose: true,
    pickerPosition: 'bottom-left',
    format: 'dd-mm-yyyy hh:ii'
});
$('.select2').select2({
    placeholder: "-- Pilih data --"
});
window.info_halaman = function(element, page, jml_halaman){
    $(".paging").remove();
    page = parseInt(page);
    var rangkai = '';
    var aktif = '';
    var batas_bawah = page - 2;
    if(batas_bawah < 1)
        batas_bawah = 1;
    for(var i = batas_bawah; i <= page; i++){
        if(i == page){
            aktif = 'kt-datatable__pager-link--active';
        }
        else{
            aktif = '';
        }
        rangkai +=
            '<li class="paging">' +
                '<a title="Page '+i+'" class="kt-datatable__pager-link kt-datatable__pager-link--prev '+aktif+'" onclick="page('+i+')">' +
                    i+
                '</a>' +
            ' </li>';
    }
    var batas_atas = page + 2;
    if(batas_atas > jml_halaman)
        batas_atas = jml_halaman;
    for(var i = (page + 1); i <= batas_atas; i++){
        rangkai +=
            '<li class="paging">' +
                '<a title="Page '+i+'" class="kt-datatable__pager-link kt-datatable__pager-link--prev" onclick="page('+i+')">' +
                    i+
                '</a>' +
            ' </li>';
    }
    $(element).after(rangkai);
}
